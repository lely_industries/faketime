# faketime

The faketime library (libft) and tools ([ft-exec] and [ft-ctrl]) allow a user to
run a command in an environment in which all or some of the system-wide real
time clocks (but not CPU-time clocks) run at a user-specified rate and with a
user-specified offset. This behavior extends to any child processes spawned by
the invoked command.

Typical use cases for faketime include:
- Generating deterministic timestamps. This may be necessary for reproducible
  builds.
- Finding time-related bugs in applications, such as improper handing of
  daylight saving time, the year 2038 problem or SSL certificate expiration.
- Speeding up test suites with explicit waits or sleeps. Since time is a
  physical quantity, adjusting the clock at the application level may lead to
  different results compared an unmodified test. faketime can speed up the clock
  without modifying the application or changing the results.

This project is heavily inspired by
[libfaketime](https://github.com/wolfcw/libfaketime). The reason for starting
this project was the need for support for more Linux system calls, accurate and
non-overflowing time arithmetic, and clock adjustments for running processes.

## Feature highlights

- Supports nearly every time-related POSIX and Linux system call.
- Supports stopped clocks.
- Per-clock adjustable rates and offsets. Fake clocks can be modified for
  running processes with `ft-ctrl`.
- Supports excluding specified processes. This allows, for example, a test suite
  to use a fake clock, but report the real runtime for the tests.
- Accurate time arithmetic with overflow checks.
- Thread-safe.

## Issues

- Does not work on statically linked or setuid binaries.
- System calls using a fake clock are no longer async-signal-safe (although not
  always, see BUGS section in the `ft-exec` man page).
- Only tested on Linux with glibc. BSD should work, but is not tested. Mac OS X
  is not supported, but this should be easy to fix.

## Installation

faketime uses the Autotools build system. To build, run:

    $ autoreconf -i
    $ ./configure
    $ make

To install, run:

    $ make install

## Documentation

See the [ft-exec](doc/ft-exec.md.in) and [ft-ctrl](doc/ft-ctrl.md.in) man pages
for documentation and exampes.

## Licensing

Copyright 2018-2022 [Lely Industries N.V.](http://www.lely.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

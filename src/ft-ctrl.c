// SPDX-License-Identifier: Apache-2.0

#include "ft.h"

#include <stdbool.h>
#include <string.h>

#ifndef CMD
#define CMD "ft-ctrl"
#endif

#define HELP \
	"Usage: " CMD " [OPTION]... [NAME=VALUE]...\n" \
	"Adjust one or more fake system-wide real time clocks.\n\n" \
	"Options:\n" \
	"  -h, --help            show this information\n" \
	"  -i, --inc=INC         increment offset of all wall clocks by INC\n" \
	"  -o, --offset=OFFSET   set offset of all wall clocks to OFFSET (equivalent to\n" \
	"                        FT_OFFSET=OFFSET)\n" \
	"  -r, --rate=RATE       set rate of all wall clocks to RATE (equivalent to \n" \
	"                        FR_RATE=RATE)\n\n" \
	"Operands of the form NAME=VALUE are interpreted as environment variables.\n" \
	"    Valid environment variables:\n" \
	"      FT_RATE           the new rate for all wall clocks, unless overridden by a\n" \
	"                        clock-specific rate\n" \
	"      FT_<CLOCK>_RATE   the new rate for CLOCK\n" \
	"      FT_OFFSET         the new offset for all wall clocks, unless overridden by\n" \
	"                        a clock-specific offset\n" \
	"      FT_<CLOCK>_OFFSET the new rate for CLOCK\n" \
	"    CLOCK is one of the following:\n" \
	"      REALTIME\n" \
	"      MONOTONIC\n" \
	"      MONOTONIC_RAW     (on Linux)\n" \
	"      REALTIME_COARSE   (on Linux)\n" \
	"      MONOTONIC_COARSE  (on Linux)\n" \
	"      BOOTTIME          (on Linux)\n" \
	"      TAI               (on Linux)"

extern char **environ;

static void ft_fini(void);

static struct ft_shm *ft_shm;

#define FT_CLOCKATTR_CTRL(attr, name) \
	ft_clockattr_ctrl((attr), "FT_" name "_RATE", "FT_" name "_OFFSET")

static void ft_clockattr_ctrl(struct ft_clockattr *attr, const char *ratename,
		const char *offsetname);

static bool ft_have_rate = false;
static double ft_rate = 1.;

static bool ft_have_offset = false;
static struct timespec ft_offset = { 0, 0 };

static bool ft_have_inc = false;
static struct timespec ft_inc = { 0, 0 };

int
main(int argc, char *argv[])
{
	char *envval = getenv("FT_SHM");
	if (envval && *envval) {
		ft_shm = ft_shm_open(envval);
		if (!ft_shm) {
			perror(envval);
			return EXIT_FAILURE;
		}
		atexit(&ft_fini);
	}

	environ = NULL;

	opterr = 0;
	int errflg = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-')
			break;
		if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "help")) {
				printf("%s\n", HELP);
				return EXIT_SUCCESS;
			} else if (!strncmp(arg, "inc=", 4)) {
				ft_have_inc = true;
				ft_inc = strtots(arg + 4, NULL);
			} else if (!strncmp(arg, "offset=", 7)) {
				setenv("FT_OFFSET", arg + 7, 1);
			} else if (!strncmp(arg, "rate=", 5)) {
				setenv("FT_RATE", arg + 5, 1);
			} else {
				fprintf(stderr, CMD ": illegal option -- %s\n",
						arg);
				errflg = 1;
			}
		} else {
			int c = getopt(argc, argv, "hi:o:r:");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr,
						CMD
						": option requires an argument -- %c\n",
						optopt);
				errflg = 1;
				break;
			case '?':
				fprintf(stderr, CMD ": illegal option -- %c\n",
						optopt);
				errflg = 1;
				break;
			case 'h': printf("%s\n", HELP); return EXIT_SUCCESS;
			case 'i':
				ft_have_inc = true;
				ft_inc = strtots(optarg, NULL);
				break;
			case 'o': setenv("FT_OFFSET", optarg, 1); break;
			case 'r': setenv("FT_RATE", optarg, 1); break;
			}
		}
	}

	while (optind < argc) {
		if (putenv(argv[optind++])) {
			perror(CMD ": error: unable to set environment variable");
			errflg = 1;
		}
	}

	if (errflg) {
		fprintf(stderr, "%s\n", HELP);
		return EXIT_FAILURE;
	}

	if (!ft_shm) {
		fprintf(stderr, CMD ": error: FT_SHM not set\n");
		return EXIT_FAILURE;
	}

	if ((envval = getenv("FT_RATE"))) {
		ft_have_rate = true;
		ft_rate = strtorate(envval, NULL);
	}

	if ((envval = getenv("FT_OFFSET"))) {
		ft_have_offset = true;
		ft_offset = strtots(envval, NULL);
	}

	struct ft_info *info = &ft_shm->info;
	ft_shm_lock(ft_shm);

	FT_CLOCKATTR_CTRL(&info->realtime, "REALTIME");
	FT_CLOCKATTR_CTRL(&info->monotonic, "MONOTONIC");
#ifdef CLOCK_MONOTONIC_RAW
	FT_CLOCKATTR_CTRL(&info->monotonic_raw, "MONOTONIC_RAW");
#endif
#ifdef CLOCK_REALTIME_COARSE
	FT_CLOCKATTR_CTRL(&info->realtime_coarse, "REALTIME_COARSE");
#endif
#ifdef CLOCK_MONOTONIC_COARSE
	FT_CLOCKATTR_CTRL(&info->monotonic_coarse, "MONOTONIC_COARSE");
#endif
#ifdef CLOCK_BOOTTIME
	FT_CLOCKATTR_CTRL(&info->boottime, "BOOTTIME");
#endif
#ifdef CLOCK_TAI
	FT_CLOCKATTR_CTRL(&info->tai, "TAI");
#endif

	ft_shm_unlock(ft_shm);

	return EXIT_SUCCESS;
}

static void
ft_fini(void)
{
	munmap(ft_shm, sizeof(*ft_shm));
}

static void
ft_clockattr_ctrl(struct ft_clockattr *attr, const char *ratename,
		const char *offsetname)
{
	assert(attr);

	char *envval;
	if ((envval = getenv(ratename))) {
		attr->rate = strtorate(envval, NULL);
	} else if (ft_have_rate) {
		attr->rate = ft_rate;
	}
	if ((envval = getenv(offsetname))) {
		attr->offset = strtots(envval, NULL);
	} else if (ft_have_offset) {
		attr->offset = ft_offset;
	} else if (ft_have_inc) {
		attr->offset = timespec_add(&attr->offset, &ft_inc);
	}
}

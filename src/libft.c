// SPDX-License-Identifier: Apache-2.0
#ifndef FT_LIBFT_C_
#define FT_LIBFT_C_

#include "ft.h"

#include <aio.h>
#include <dlfcn.h>
#include <fenv.h>
#include <mqueue.h>
#include <poll.h>
// #include <sched.h>
#include <semaphore.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/timeb.h>
// #include <sys/times.h>
#include <utime.h>

#ifdef __GLIBC__
#include <netdb.h>
#include <sys/sem.h>
#endif

#ifdef __linux__
#include <linux/sockios.h>
#include <sys/epoll.h>
// Ignore variadic ioctl() definition.
#define ioctl hide_ioctl
#include <sys/ioctl.h>
#undef ioctl
#include <sys/timerfd.h>
#include <sys/timex.h>
#endif

#ifndef __GLIBC_PREREQ
#define __GLIBC_PREREQ(major, minor) 0
#endif

#if !defined(_Thread_local) && !(__STDC_VERSION__ >= 201112L)
#ifdef __GNUC__
#define _Thread_local __thread
#else
#define _Thread_local
#endif
#endif

#ifndef TIMESPEC_MIN
#define TIMESPEC_MIN \
	(struct timespec) { TIME_T_MIN, 0 }
#endif

#ifndef TIMESPEC_MAX
#define TIMESPEC_MAX \
	(struct timespec) { TIME_T_MAX, 999999999l }
#endif

#ifndef TIMEVAL_MIN
#define TIMEVAL_MIN \
	(struct timeval) { TIME_T_MIN, 0 }
#endif

#ifndef TIMEVAL_MAX
#define TIMEVAL_MAX \
	(struct timeval) { TIME_T_MAX, 999999l }
#endif

#ifndef FT_MAX_FD
#define FT_MAX_FD FD_SETSIZE
#endif

#ifdef __GNUC__
static void ft_init(void) __attribute__((constructor));
static void ft_fini(void) __attribute__((destructor));
#else
static void ft_init(void);
#endif
static void ft_init_routine(void);

static bool ft_skip = false;
static _Thread_local bool ft_fake = true;

static bool ft_no_clockid(clockid_t clockid);

static bool ft_no_realtime = false;
static bool ft_no_monotonic = false;
#ifdef CLOCK_MONOTONIC_RAW
static bool ft_no_monotonic_raw = false;
#endif
#ifdef CLOCK_REALTIME_COARSE
static bool ft_no_realtime_coarse = false;
#endif
#ifdef CLOCK_MONOTONIC_COARSE
static bool ft_no_monotonic_coarse = false;
#endif
#ifdef CLOCK_BOOTTIME
static bool ft_no_boottime = false;
#endif
#ifdef CLOCK_TAI
static bool ft_no_tai = false;
#endif
static bool ft_no_file = false;
static bool ft_no_settime = false;

#define FT_TRACE_ALL() FT_TRACE(TRACE_ALL)

#define FT_TRACE_FAKE() FT_TRACE(TRACE_FAKE)

#define FT_TRACE(trace) \
	do { \
		if (ft_trace == (trace)) \
			ft_print_trace(__func__); \
	} while (0)

static void ft_print_trace(const char *func);

enum { TRACE_NONE, TRACE_ALL, TRACE_FAKE };
static int ft_trace = TRACE_NONE;

static void ft_clockattr_load(struct ft_clockattr *attr, clockid_t clockid);

static struct ft_shm *ft_shm;
static struct ft_info *ft_info;

#define FT_BLOCK() \
	sigset_t ft_oset; \
	if (ft_safe) \
		pthread_sigmask(SIG_SETMASK, &ft_set, &ft_oset);

#define FT_UNBLOCK() \
	if (ft_safe) \
		pthread_sigmask(SIG_SETMASK, &ft_oset, NULL);

static bool ft_safe = false;
static sigset_t ft_set;

static pthread_mutex_t ft_pthread_cond_mtx = PTHREAD_MUTEX_INITIALIZER;
static struct ft_pthread_cond_node {
	pthread_cond_t *cond;
	clockid_t clockid;
	struct ft_pthread_cond_node *next;
} *ft_pthread_cond_list = NULL;

static bool ft_timer_find(timer_t timerid, clockid_t *pclockid);
#ifdef __GLIBC__
static bool ft_timer_find_225(int timerid, clockid_t *pclockid);
#endif

static pthread_mutex_t ft_timer_mtx = PTHREAD_MUTEX_INITIALIZER;
static struct ft_timer_node {
	timer_t timerid;
	clockid_t clockid;
	struct ft_timer_node *next;
} *ft_timer_list = NULL;
#ifdef __GLIBC__
static struct ft_timer_node_225 {
	int timerid;
	clockid_t clockid;
	struct ft_timer_node_225 *next;
} *ft_timer_list_225 = NULL;
#endif

#ifdef __linux__
static clockid_t ft_timerfd_clockid[FT_MAX_FD];
#endif

static int ft_timeout_fake2real(int timeout, const struct ft_clockattr *attr);

static struct timespec ft_timespec_fake2real_utime(
		const struct timespec *tp, const struct ft_clockattr *attr);

static struct timespec ft_timespec_fake2real_abs(
		const struct timespec *tp, const struct ft_clockattr *attr);
static struct timespec ft_timespec_real2fake_abs(
		const struct timespec *tp, const struct ft_clockattr *attr);
static struct timespec ft_timespec_fake2real_rel(
		const struct timespec *tp, const struct ft_clockattr *attr);
static struct timespec ft_timespec_real2fake_rel(
		const struct timespec *tp, const struct ft_clockattr *attr);

static struct timeval ft_timeval_fake2real_abs(
		const struct timeval *tp, const struct ft_clockattr *attr);
static struct timeval ft_timeval_real2fake_abs(
		const struct timeval *tp, const struct ft_clockattr *attr);
static struct timeval ft_timeval_fake2real_rel(
		const struct timeval *tp, const struct ft_clockattr *attr);
static struct timeval ft_timeval_real2fake_rel(
		const struct timeval *tp, const struct ft_clockattr *attr);

// From <aio.h>:
static int (*next_aio_suspend)(const struct aiocb *const list[], int nent,
		const struct timespec *timeout);
#ifdef __GLIBC__
static int (*next_aio_suspend64)(const struct aiocb64 *const list[], int nent,
		const struct timespec *timeout);
#endif

// From <fcntl.h>:
static int (*next_utimensat)(int fd, const char *path,
		const struct timespec times[2], int flag);

// From <mqueue.h>:
static ssize_t (*next_mq_timedreceive)(mqd_t mqdes, char *msg_ptr,
		size_t msg_len, unsigned *msg_prio,
		const struct timespec *abstime);
static int (*next_mq_timedsend)(mqd_t mqdes, const char *msg_ptr,
		size_t msg_len, unsigned msg_prio,
		const struct timespec *abstime);

// From <poll.h>:
static int (*next_poll)(struct pollfd fds[], nfds_t nfds, int timeout);
#ifdef __linux__
static int (*next_ppoll)(struct pollfd *fds, nfds_t nfds,
		const struct timespec *tmo_p, const sigset_t *sigmask);
#endif

// From <pthread.h>:
static int (*next_pthread_cond_destroy)(pthread_cond_t *cond);
static int (*next_pthread_cond_init)(
		pthread_cond_t *cond, const pthread_condattr_t *attr);
static int (*next_pthread_cond_timedwait)(pthread_cond_t *cond,
		pthread_mutex_t *mutex, const struct timespec *abstime);
#ifdef __GLIBC__
static int (*next_pthread_cond_timedwait_225)(pthread_cond_t *cond,
		pthread_mutex_t *mutex, const struct timespec *abstime);
#endif
static int (*next_pthread_mutex_timedlock)(
		pthread_mutex_t *mutex, const struct timespec *abstime);
static int (*next_pthread_rwlock_timedrdlock)(
		pthread_rwlock_t *rwlock, const struct timespec *abstime);
static int (*next_pthread_rwlock_timedwrlock)(
		pthread_rwlock_t *rwlock, const struct timespec *abstime);
#ifdef __GLIBC__
static int (*next_pthread_timedjoin_np)(pthread_t thread, void **retval,
		const struct timespec *abstime);
#endif

// From <sched.h>
// static int (*next_sched_rr_get_interval)(pid_t pid, struct timespec
// *interval);

// From <semaphore.h>:
static int (*next_sem_timedwait)(sem_t *sem, const struct timespec *abstime);

// From <signal.h>
static int (*next_sigtimedwait)(const sigset_t *set, siginfo_t *info,
		const struct timespec *timeout);

// From <sys/select.h>:
static int (*next_pselect)(int nfds, fd_set *readfds, fd_set *writefds,
		fd_set *errorfds, const struct timespec *timeout,
		const sigset_t *sigmask);
static int (*next_select)(int nfds, fd_set *readfds, fd_set *writefds,
		fd_set *errorfds, struct timeval *timeout);

// From <sys/socket.h>
static int (*next_getsockopt)(int socket, int level, int option_name,
		void *option_value, socklen_t *option_len);
static int (*next_setsockopt)(int socket, int level, int option_name,
		const void *option_value, socklen_t option_len);

// From <sys/stat.h>:
#ifdef __GLIBC__
static int (*next_fstat)(int ver, int fildes, struct stat *buf);
static int (*next_fstatat)(
		int ver, int fd, const char *path, struct stat *buf, int flag);
#else
static int (*next_fstat)(int fildes, struct stat *buf);
static int (*next_fstatat)(
		int fd, const char *path, struct stat *buf, int flag);
#endif
static int (*next_futimens)(int fd, const struct timespec times[2]);
#ifdef __GLIBC__
static int (*next_lstat)(int ver, const char *path, struct stat *buf);
static int (*next_stat)(int ver, const char *path, struct stat *buf);
static int (*next_fstat64)(int ver, int fildes, struct stat64 *buf);
static int (*next_fstatat64)(int ver, int fd, const char *path,
		struct stat64 *buf, int flag);
static int (*next_lstat64)(int ver, const char *path, struct stat64 *buf);
static int (*next_stat64)(int ver, const char *path, struct stat64 *buf);
#else
static int (*next_lstat)(const char *path, struct stat *buf);
static int (*next_stat)(const char *path, struct stat *buf);
#endif

// From <sys/time.h>:
static int (*next_getitimer)(int which, struct itimerval *value);
#ifdef __GLIBC__
static int (*next_gettimeofday)(struct timeval *tp, struct timezone *tzp);
#else
static int (*next_gettimeofday)(struct timeval *tp, void *tzp);
#endif
static int (*next_setitimer)(int which, const struct itimerval *value,
		struct itimerval *ovalue);
#ifdef __GLIBC__
static int (*next_settimeofday)(
		const struct timeval *tv, const struct timezone *tz);
static int (*next_adjtime)(
		const struct timeval *delta, struct timeval *olddelta);
static int (*next_futimes)(int fd, const struct timeval tv[2]);
static int (*next_lutimes)(const char *filename, const struct timeval tv[2]);
static int (*next_futimesat)(
		int dirfd, const char *pathname, const struct timeval times[2]);
#endif
static int (*next_utimes)(const char *path, const struct timeval times[2]);

// From <sys/timeb.h>:
static int (*next_ftime)(struct timeb *tp);

// From <sys/times.h>:
// static clock_t (*next_times)(struct tms *buffer);

// From <time.h>:
// static clock_t (*next_clock)(void);
static int (*next_clock_getres)(clockid_t clock_id, struct timespec *res);
static int (*next_clock_gettime)(clockid_t clock_id, struct timespec *tp);
static int (*next_clock_nanosleep)(clockid_t clock_id, int flags,
		const struct timespec *rqtp, struct timespec *rmtp);
static int (*next_clock_settime)(clockid_t clock_id, const struct timespec *tp);
static int (*next_nanosleep)(
		const struct timespec *rqtp, struct timespec *rmtp);
static time_t (*next_time)(time_t *tloc);
#if __STDC_VERSION__ >= 201112L
static int (*next_timespec_get)(struct timespec *ts, int base);
#endif
static int (*next_timer_create)(
		clockid_t clockid, struct sigevent *evp, timer_t *timerid);
static int (*next_timer_delete)(timer_t timerid);
static int (*next_timer_gettime)(timer_t timerid, struct itimerspec *value);
static int (*next_timer_settime)(timer_t timerid, int flags,
		const struct itimerspec *value, struct itimerspec *ovalue);
#ifdef __GLIBC__
static int (*next_timer_create_225)(
		clockid_t clockid, struct sigevent *evp, int *timerid);
static int (*next_timer_delete_225)(int timerid);
static int (*next_timer_gettime_225)(int timerid, struct itimerspec *value);
static int (*next_timer_settime_225)(int timerid, int flags,
		const struct itimerspec *value, struct itimerspec *ovalue);
#endif

// From <unistd.h>:
static unsigned (*next_alarm)(unsigned seconds);
static unsigned (*next_sleep)(unsigned seconds);
static useconds_t (*next_ualarm)(useconds_t useconds, useconds_t interval);
static int (*next_usleep)(useconds_t useconds);

// From <utime.h>:
static int (*next_utime)(const char *path, const struct utimbuf *times);

#ifdef __GLIBC__

// From <netdb.h>
static int (*next_gai_suspend)(const struct gaicb *const list[], int nitems,
		const struct timespec *timeout);

// From <sys/sem.h>
static int (*next_semtimedop)(int semid, struct sembuf *sops, size_t nsops,
		const struct timespec *timeout);

#endif // __GLIBC__

#ifdef __linux__

// From <sys/epoll.h>:
static int (*next_epoll_wait)(int epfd, struct epoll_event *events,
		int maxevents, int timeout);
static int (*next_epoll_pwait)(int epfd, struct epoll_event *events,
		int maxevents, int timeout, const sigset_t *sigmask);

// From <sys/ioctl.h>
static int (*next_ioctl)(int fd, unsigned long request, ...);

// From <sys/socket.h>
static int (*next_recvmmsg)(int sockfd, struct mmsghdr *msgvec,
		unsigned int vlen, int flags,
#if __GLIBC__ == 2 && __GLIBC_MINOR__ < 21
		const struct timespec *timeout);
#else
		struct timespec *timeout);
#endif

// From <sys/timerfd.h>:
static int (*next_timerfd_create)(int clockid, int flags);
static int (*next_timerfd_settime)(int fd, int flags,
		const struct itimerspec *new_value,
		struct itimerspec *old_value);
static int (*next_timerfd_gettime)(int fd, struct itimerspec *curr_value);

// From <sys/timex.h>
static int (*next_adjtimex)(struct timex *buf);
static int (*next_ntp_adjtime)(struct timex *buf);
// static int (*next_ntp_gettime)(struct ntptimeval *ntv);
// static int (*next_ntp_gettimex)(struct ntptimeval *ntv);

#endif // __linux__

static struct timeval timespec2val(const struct timespec *tp);

static struct timespec timespec_sub(
		const struct timespec *tp, const struct timespec *dec);
static struct timespec timespec_mul(const struct timespec *tp, double mul);
static struct timespec timespec_div(const struct timespec *tp, double mul);

static struct timeval timeval_add(
		const struct timeval *tp, const struct timeval *inc);
static struct timeval timeval_sub(
		const struct timeval *tp, const struct timeval *dec);
static struct timeval timeval_mul(const struct timeval *tp, double mul);
static struct timeval timeval_div(const struct timeval *tp, double mul);

static int
fake_clock_gettime(clockid_t clock_id, struct timespec *tp)
{
	assert(next_clock_gettime);

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clock_id);

	FT_BLOCK();
	ft_fake = false;
	int result = next_clock_gettime(clock_id, tp);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		*tp = ft_timespec_real2fake_abs(tp, &attr);

	return result;
}

static int
fake_clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *rqtp,
		struct timespec *rmtp)
{
	assert(next_clock_nanosleep);

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clock_id);

	struct timespec next_rqtp = (flags & TIMER_ABSTIME)
			? ft_timespec_fake2real_abs(rqtp, &attr)
			: ft_timespec_fake2real_rel(rqtp, &attr);

	ft_fake = false;
	int result = next_clock_nanosleep(clock_id, flags, &next_rqtp, rmtp);
	ft_fake = true;

	if (result == -1 && errno == EINTR && rmtp)
		*rmtp = ft_timespec_real2fake_abs(rmtp, &attr);

	return result;
}

int
aio_suspend(const struct aiocb *const list[], int nent,
		const struct timespec *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_aio_suspend) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_aio_suspend(list, nent, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	FT_BLOCK();
	ft_fake = false;
	int result = next_aio_suspend(list, nent, &next_timeout);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

#ifdef __GLIBC__
int
aio_suspend64(const struct aiocb64 *const list[], int nent,
		const struct timespec *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_aio_suspend64) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_aio_suspend64(list, nent, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	FT_BLOCK();
	ft_fake = false;
	int result = next_aio_suspend64(list, nent, &next_timeout);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}
#endif // __GLIBC__

int
utimensat(int fd, const char *path, const struct timespec times[2], int flag)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_utimensat) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !times) {
		return next_utimensat(fd, path, times, flag);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timespec next_times[2] = {
		ft_timespec_fake2real_utime(&times[0], &attr),
		ft_timespec_fake2real_utime(&times[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_utimensat(fd, path, next_times, flag);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

ssize_t
mq_timedreceive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned *msg_prio,
		const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_mq_timedreceive) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_mq_timedreceive(
				mqdes, msg_ptr, msg_len, msg_prio, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	ssize_t result = next_mq_timedreceive(
			mqdes, msg_ptr, msg_len, msg_prio, &next_abstime);
	ft_fake = true;

	return result;
}

int
mq_timedsend(mqd_t mqdes, const char *msg_ptr, size_t msg_len,
		unsigned msg_prio, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_mq_timedsend) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_mq_timedsend(
				mqdes, msg_ptr, msg_len, msg_prio, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_mq_timedsend(
			mqdes, msg_ptr, msg_len, msg_prio, &next_abstime);
	ft_fake = true;

	return result;
}

int
poll(struct pollfd fds[], nfds_t nfds, int timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_poll) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || timeout <= 0) {
		return next_poll(fds, nfds, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	int next_timeout = ft_timeout_fake2real(timeout, &attr);

	ft_fake = false;
	int result = next_poll(fds, nfds, next_timeout);
	ft_fake = true;

	return result;
}

#ifdef __linux__
int
ppoll(struct pollfd *fds, nfds_t nfds, const struct timespec *tmo_p,
		const sigset_t *sigmask)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_ppoll) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !tmo_p) {
		return next_ppoll(fds, nfds, tmo_p, sigmask);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timespec next_tmo_p = ft_timespec_fake2real_rel(tmo_p, &attr);

	ft_fake = false;
	int result = next_ppoll(fds, nfds, &next_tmo_p, sigmask);
	ft_fake = true;

	return result;
}
#endif // __linux__

int
#ifdef __GLIBC__
pthread_cond_destroy_232(pthread_cond_t *cond)
#else
pthread_cond_destroy(pthread_cond_t *cond)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_cond_destroy)
		return ENOSYS;

	struct ft_pthread_cond_node *node = NULL;
	while (pthread_mutex_lock(&ft_pthread_cond_mtx) == EINTR)
		;
	for (struct ft_pthread_cond_node **pnode = &ft_pthread_cond_list;
			(node = *pnode); pnode = &(*pnode)->next) {
		if ((*pnode)->cond == cond) {
			*pnode = (*pnode)->next;
			break;
		}
	}
	pthread_mutex_unlock(&ft_pthread_cond_mtx);
	free(node);

	return next_pthread_cond_destroy(cond);
}
#ifdef __GLIBC__
__asm__(".symver pthread_cond_destroy_232, pthread_cond_destroy@@GLIBC_2.3.2");
#endif

int
#ifdef __GLIBC__
pthread_cond_init_232(pthread_cond_t *cond, const pthread_condattr_t *attr)
#else
pthread_cond_init(pthread_cond_t *cond, const pthread_condattr_t *attr)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_cond_init)
		return ENOSYS;

	int result = next_pthread_cond_init(cond, attr);

	if (!ft_skip && !result && attr) {
		clockid_t clockid = CLOCK_REALTIME;
		pthread_condattr_getclock(attr, &clockid);
		if (!ft_no_clockid(clockid)) {
			int errsv = errno;
			struct ft_pthread_cond_node *node =
					malloc(sizeof(*node));
			if (!node) {
				result = errno;
				assert(next_pthread_cond_destroy);
				next_pthread_cond_destroy(cond);
				errno = errsv;
				return result;
			}

			node->cond = cond;
			node->clockid = clockid;

			while (pthread_mutex_lock(&ft_pthread_cond_mtx)
					== EINTR)
				;
			node->next = ft_pthread_cond_list;
			ft_pthread_cond_list = node;
			pthread_mutex_unlock(&ft_pthread_cond_mtx);
		}
	}

	return result;
}
#ifdef __GLIBC__
__asm__(".symver pthread_cond_init_232, pthread_cond_init@@GLIBC_2.3.2");
#endif

int
#ifdef __GLIBC__
pthread_cond_timedwait_232(pthread_cond_t *cond, pthread_mutex_t *mutex,
		const struct timespec *abstime)
#else
pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex,
		const struct timespec *abstime)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_cond_timedwait) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake) {
		return next_pthread_cond_timedwait(cond, mutex, abstime);
	}
	FT_TRACE_FAKE();

	struct timespec next_abstime;
	while (pthread_mutex_lock(&ft_pthread_cond_mtx) == EINTR)
		;
	struct ft_pthread_cond_node *node;
	for (node = ft_pthread_cond_list; node; node = node->next) {
		if (node->cond == cond)
			break;
	}
	if (node) {
		clockid_t clockid = node->clockid;
		pthread_mutex_unlock(&ft_pthread_cond_mtx);
		assert(!ft_no_clockid(clockid));
		struct ft_clockattr attr;
		ft_clockattr_load(&attr, clockid);
		next_abstime = ft_timespec_fake2real_abs(abstime, &attr);
	} else {
		pthread_mutex_unlock(&ft_pthread_cond_mtx);
		next_abstime = *abstime;
	}

	ft_fake = false;
	int result = next_pthread_cond_timedwait(cond, mutex, &next_abstime);
	ft_fake = true;

	return result;
}
#ifdef __GLIBC__
__asm__(".symver pthread_cond_timedwait_232, pthread_cond_timedwait@@GLIBC_2.3.2");
#endif

#ifdef __GLIBC__
int
pthread_cond_timedwait_225(pthread_cond_t *cond, pthread_mutex_t *mutex,
		const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_cond_timedwait) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake) {
		return next_pthread_cond_timedwait_225(cond, mutex, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_pthread_cond_timedwait_225(
			cond, mutex, &next_abstime);
	ft_fake = true;

	return result;
}
__asm__(".symver pthread_cond_timedwait_225, pthread_cond_timedwait@GLIBC_2.2.5");
#endif // __GLIBC__

int
pthread_mutex_timedlock(pthread_mutex_t *mutex, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_mutex_timedlock) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake) {
		return next_pthread_mutex_timedlock(mutex, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_pthread_mutex_timedlock(mutex, &next_abstime);
	ft_fake = true;

	return result;
}

int
pthread_rwlock_timedrdlock(
		pthread_rwlock_t *rwlock, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_rwlock_timedrdlock) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake) {
		return next_pthread_rwlock_timedrdlock(rwlock, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_pthread_rwlock_timedrdlock(rwlock, &next_abstime);
	ft_fake = true;

	return result;
}

int
pthread_rwlock_timedwrlock(
		pthread_rwlock_t *rwlock, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_rwlock_timedwrlock) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake) {
		return next_pthread_rwlock_timedwrlock(rwlock, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_pthread_rwlock_timedwrlock(rwlock, &next_abstime);
	ft_fake = true;

	return result;
}

#ifdef __GLIBC__
int
pthread_timedjoin_np(
		pthread_t thread, void **retval, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pthread_timedjoin_np) {
		return ENOSYS;
	} else if (ft_skip || !ft_fake || !abstime) {
		return next_pthread_timedjoin_np(thread, retval, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_pthread_timedjoin_np(thread, retval, &next_abstime);
	ft_fake = true;

	return result;
}
#endif // __GLIBC__

int
sem_timedwait(sem_t *sem, const struct timespec *abstime)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_sem_timedwait) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_sem_timedwait(sem, abstime);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_abstime =
			ft_timespec_fake2real_abs(abstime, &attr);

	ft_fake = false;
	int result = next_sem_timedwait(sem, &next_abstime);
	ft_fake = true;

	return result;
}

int
sigtimedwait(const sigset_t *set, siginfo_t *info,
		const struct timespec *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_sigtimedwait) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_sigtimedwait(set, info, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_sigtimedwait(set, info, &next_timeout);
	ft_fake = true;

	return result;
}

int
pselect(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds,
		const struct timespec *timeout, const sigset_t *sigmask)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_pselect) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_pselect(nfds, readfds, writefds, errorfds, timeout,
				sigmask);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_pselect(nfds, readfds, writefds, errorfds,
			&next_timeout, sigmask);
	ft_fake = true;

	return result;
}

int
select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds,
		struct timeval *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_select) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_select(nfds, readfds, writefds, errorfds, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timeval next_timeout = ft_timeval_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_select(
			nfds, readfds, writefds, errorfds, &next_timeout);
	ft_fake = true;

	*timeout = ft_timeval_real2fake_rel(&next_timeout, &attr);

	return result;
}

int
getsockopt(int socket, int level, int option_name, void *option_value,
		socklen_t *option_len)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_getsockopt) {
		errno = ENOSYS;
		return -1;
		// clang-format off
	} else if (ft_skip || !ft_fake || (option_name != SO_LINGER
			&& option_name != SO_RCVTIMEO
			&& option_name != SO_SNDTIMEO) || !option_value) {
		// clang-format on
		return next_getsockopt(socket, level, option_name, option_value,
				option_len);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	FT_BLOCK();
	ft_fake = false;
	int result = next_getsockopt(
			socket, level, option_name, option_value, option_len);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result) {
		switch (option_name) {
		case SO_LINGER: {
			struct linger *lp = option_value;
			if (lp->l_linger > 0) {
				struct timeval tv = { lp->l_linger, 0 };
				tv = ft_timeval_real2fake_rel(&tv, &attr);
				lp->l_linger = tv.tv_sec > 0 ? tv.tv_sec : 1;
			}
			break;
		}
		case SO_RCVTIMEO:
		case SO_SNDTIMEO: {
			struct timeval *tp = option_value;
			*tp = ft_timeval_real2fake_rel(tp, &attr);
			break;
		}
		}
	}

	return result;
}

int
setsockopt(int socket, int level, int option_name, const void *option_value,
		socklen_t option_len)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_setsockopt) {
		errno = ENOSYS;
		return -1;
		// clang-format off
	} else if (ft_skip || !ft_fake || (option_name != SO_LINGER
			&& option_name != SO_RCVTIMEO
			&& option_name != SO_SNDTIMEO) || !option_value) {
		// clang-format on
		return next_setsockopt(socket, level, option_name, option_value,
				option_len);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	const void *next_option_value = option_value;
	struct linger l = { 0, 0 };
	struct timeval tv = { 0, 0 };
	switch (option_name) {
	case SO_LINGER:
		l = *(const struct linger *)option_value;
		if (l.l_linger > 0) {
			tv.tv_sec = l.l_linger;
			tv = ft_timeval_fake2real_rel(&tv, &attr);
			l.l_linger = tv.tv_sec > 0 ? tv.tv_sec : 1;
			next_option_value = &l;
		}
		break;
	case SO_RCVTIMEO:
	case SO_SNDTIMEO:
		tv = *(const struct timeval *)option_value;
		tv = ft_timeval_fake2real_rel(&tv, &attr);
		next_option_value = &tv;
		break;
	}

	FT_BLOCK();
	ft_fake = false;
	int result = next_setsockopt(socket, level, option_name,
			next_option_value, option_len);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

#define FT_STAT_REAL2FAKE(buf, attr) \
	do { \
		(buf)->st_atim = ft_timespec_real2fake_abs( \
				&(buf)->st_atim, (attr)); \
		(buf)->st_mtim = ft_timespec_real2fake_abs( \
				&(buf)->st_mtim, (attr)); \
		(buf)->st_ctim = ft_timespec_real2fake_abs( \
				&(buf)->st_ctim, (attr)); \
	} while (0)

int
#ifdef __GLIBC__
__fxstat(int ver, int fildes, struct stat *buf)
#else
fstat(int fildes, struct stat *buf)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_fstat) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
#ifdef __GLIBC__
		return next_fstat(ver, fildes, buf);
#else
		return next_fstat(fildes, buf);
#endif
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
#ifdef __GLIBC__
	int result = next_fstat(ver, fildes, buf);
#else
	int result = next_fstat(fildes, buf);
#endif
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
#ifdef __GLIBC__
__fxstatat(int ver, int fd, const char *path, struct stat *buf, int flag)
#else
fstatat(int fd, const char *path, struct stat *buf, int flag)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_fstatat) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
#ifdef __GLIBC__
		return next_fstatat(ver, fd, path, buf, flag);
#else
		return next_fstatat(fd, path, buf, flag);
#endif
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
#ifdef __GLIBC__
	int result = next_fstatat(ver, fd, path, buf, flag);
#else
	int result = next_fstatat(fd, path, buf, flag);
#endif
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
futimens(int fd, const struct timespec times[2])
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_futimens) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !times) {
		return next_futimens(fd, times);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timespec next_times[2] = {
		ft_timespec_fake2real_utime(&times[0], &attr),
		ft_timespec_fake2real_utime(&times[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_futimens(fd, next_times);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

int
#ifdef __GLIBC__
__lxstat(int ver, const char *path, struct stat *buf)
#else
lstat(const char *path, struct stat *buf)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_lstat) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
#ifdef __GLIBC__
		return next_lstat(ver, path, buf);
#else
		return next_lstat(path, buf);
#endif
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
#ifdef __GLIBC__
	int result = next_lstat(ver, path, buf);
#else
	int result = next_lstat(path, buf);
#endif
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
#ifdef __GLIBC__
__xstat(int ver, const char *path, struct stat *buf)
#else
stat(const char *path, struct stat *buf)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_stat) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
#ifdef __GLIBC__
		return next_stat(ver, path, buf);
#else
		return next_stat(path, buf);
#endif
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
#ifdef __GLIBC__
	int result = next_stat(ver, path, buf);
#else
	int result = next_stat(path, buf);
#endif
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

#ifdef __GLIBC__

int
__fxstat64(int ver, int fildes, struct stat64 *buf)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_fstat64) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
		return next_fstat64(ver, fildes, buf);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
	int result = next_fstat64(ver, fildes, buf);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
__fxstatat64(int ver, int fd, const char *path, struct stat64 *buf, int flag)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_fstatat64) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
		return next_fstatat64(ver, fd, path, buf, flag);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
	int result = next_fstatat64(ver, fd, path, buf, flag);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
__lxstat64(int ver, const char *path, struct stat64 *buf)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_lstat64) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
		return next_lstat64(ver, path, buf);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
	int result = next_lstat64(ver, path, buf);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

int
__xstat64(int ver, const char *path, struct stat64 *buf)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_stat64) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file) {
		return next_stat64(ver, path, buf);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
	int result = next_stat64(ver, path, buf);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result)
		FT_STAT_REAL2FAKE(buf, &attr);

	return result;
}

#endif // __GLIBC__

#undef FT_STAT_REAL2FAKE

int
#ifdef __GLIBC__
getitimer(__itimer_which_t which, struct itimerval *value)
#else
getitimer(int which, struct itimerval *value)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_getitimer) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || which != ITIMER_REAL || !value) {
		return next_getitimer(which, value);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	ft_fake = false;
	int result = next_getitimer(which, value);
	ft_fake = true;

	if (!result) {
		value->it_interval = ft_timeval_real2fake_rel(
				&value->it_interval, &attr);
		value->it_value = ft_timeval_real2fake_rel(
				&value->it_value, &attr);
	}

	return result;
}

int
#if !defined(__GLIBC__) || __GLIBC_PREREQ(2, 31)
gettimeofday(struct timeval *tp, void *tzp)
#else
gettimeofday(struct timeval *tp, struct timezone *tzp)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_gettimeofday) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_gettimeofday(tp, tzp);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	ft_fake = false;
	int result = next_gettimeofday(tp, tzp);
	ft_fake = true;

	*tp = ft_timeval_real2fake_abs(tp, &attr);

	return result;
}

int
#ifdef __GLIBC__
setitimer(__itimer_which_t which, const struct itimerval *value,
		struct itimerval *ovalue)
#else
setitimer(int which, const struct itimerval *value, struct itimerval *ovalue)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_setitimer) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || which != ITIMER_REAL) {
		return next_setitimer(which, value, ovalue);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct itimerval next_value = { { 0 }, { 0 } };
	if (value) {
		next_value.it_interval = ft_timeval_fake2real_rel(
				&value->it_interval, &attr);
		next_value.it_value = ft_timeval_fake2real_rel(
				&value->it_value, &attr);
	}

	ft_fake = false;
	int result = next_setitimer(which, value ? &next_value : NULL, ovalue);
	ft_fake = true;

	if (!result && ovalue) {
		ovalue->it_interval = ft_timeval_real2fake_rel(
				&ovalue->it_interval, &attr);
		ovalue->it_value = ft_timeval_real2fake_rel(
				&ovalue->it_value, &attr);
	}

	return result;
}

#ifdef __GLIBC__

int
settimeofday(const struct timeval *tv, const struct timezone *tz)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_settimeofday) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !tv) {
		return next_settimeofday(tv, tz);
	} else if (ft_no_settime) {
		errno = EPERM;
		return -1;
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timeval next_tv = ft_timeval_fake2real_abs(tv, &attr);

	ft_fake = false;
	int result = next_settimeofday(&next_tv, tz);
	ft_fake = true;

	return result;
}

int
adjtime(const struct timeval *delta, struct timeval *olddelta)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_adjtime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_adjtime(delta, olddelta);
	} else {
		// We do not support fake clock adjustments.
		errno = EPERM;
		return -1;
	}
}

int
futimes(int fd, const struct timeval tv[2])
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_futimes) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !tv) {
		return next_futimes(fd, tv);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timeval next_tv[2] = {
		ft_timeval_fake2real_abs(&tv[0], &attr),
		ft_timeval_fake2real_abs(&tv[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_futimes(fd, next_tv);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

int
lutimes(const char *filename, const struct timeval tv[2])
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_lutimes) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !tv) {
		return next_lutimes(filename, tv);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timeval next_tv[2] = {
		ft_timeval_fake2real_abs(&tv[0], &attr),
		ft_timeval_fake2real_abs(&tv[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_lutimes(filename, next_tv);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

int
futimesat(int dirfd, const char *pathname, const struct timeval times[2])
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_lutimes) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !times) {
		return next_futimesat(dirfd, pathname, times);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timeval next_times[2] = {
		ft_timeval_fake2real_abs(&times[0], &attr),
		ft_timeval_fake2real_abs(&times[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_futimesat(dirfd, pathname, next_times);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

#endif // __GLIBC__

int
utimes(const char *path, const struct timeval times[2])
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_utimes) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !times) {
		return next_utimes(path, times);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct timeval next_times[2] = {
		ft_timeval_fake2real_abs(&times[0], &attr),
		ft_timeval_fake2real_abs(&times[1], &attr)
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_utimes(path, next_times);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

int
ftime(struct timeb *tp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_ftime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_ftime(tp);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	FT_BLOCK();
	ft_fake = false;
	int result = next_ftime(tp);
	ft_fake = true;
	FT_UNBLOCK();

	if (!result) {
		struct timespec tmp = ft_timespec_real2fake_abs(
				&(struct timespec){ tp->time,
						(long)tp->millitm * 1000000l },
				&attr);
		tp->time = tmp.tv_sec;
		tp->millitm = tmp.tv_nsec / 1000000l;
	}

	return result;
}

int
clock_getres(clockid_t clock_id, struct timespec *tp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_clock_getres) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_clockid(clock_id) || !tp) {
		return next_clock_getres(clock_id, tp);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clock_id);

	ft_fake = false;
	int result = next_clock_getres(clock_id, tp);
	ft_fake = true;

	if (!result)
		*tp = ft_timespec_real2fake_rel(tp, &attr);

	return result;
}

int
clock_gettime(clockid_t clock_id, struct timespec *tp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_clock_gettime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_clockid(clock_id) || !tp) {
		return next_clock_gettime(clock_id, tp);
	}
	FT_TRACE_FAKE();

	return fake_clock_gettime(clock_id, tp);
}

int
clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *rqtp,
		struct timespec *rmtp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_clock_nanosleep) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_clockid(clock_id)) {
		return next_clock_nanosleep(clock_id, flags, rqtp, rmtp);
	}
	FT_TRACE_FAKE();

	return fake_clock_nanosleep(clock_id, flags, rqtp, rmtp);
}

int
clock_settime(clockid_t clock_id, const struct timespec *tp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_clock_settime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_clockid(clock_id) || !tp) {
		return next_clock_settime(clock_id, tp);
	} else if (ft_no_settime) {
		errno = EPERM;
		return -1;
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clock_id);

	struct timespec next_tp = ft_timespec_fake2real_abs(tp, &attr);

	ft_fake = false;
	int result = next_clock_settime(clock_id, &next_tp);
	ft_fake = true;

	return result;
}

int
nanosleep(const struct timespec *rqtp, struct timespec *rmtp)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_nanosleep && (ft_skip || !ft_fake)) {
		return next_nanosleep(rqtp, rmtp);
	} else if (!next_clock_nanosleep) {
		errno = ENOSYS;
		return -1;
	}
	FT_TRACE_FAKE();

	return fake_clock_nanosleep(CLOCK_REALTIME, 0, rqtp, rmtp);
}

time_t
time(time_t *tloc)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_time && (ft_skip || !ft_fake)) {
		return next_time(tloc);
	} else if (!next_clock_gettime) {
		errno = ENOSYS;
		return -1;
	}
	FT_TRACE_FAKE();

	struct timespec tp = { 0, 0 };
	int result = fake_clock_gettime(CLOCK_REALTIME, &tp);

	return result ? result : tp.tv_sec;
}

#if __STDC_VERSION__ >= 201112L
int
timespec_get(struct timespec *ts, int base)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_timespec_get && (ft_skip || !ft_fake || base != TIME_UTC)) {
		return next_timespec_get(ts, base);
	} else if (!next_clock_gettime) {
		errno = ENOSYS;
		return 0;
	}
	FT_TRACE_FAKE();

	return fake_clock_gettime(CLOCK_REALTIME, ts) ? 0 : base;
}
#endif // __STDC_VERSION__ >= 201112L

int
#ifdef __GLIBC__
timer_create_233(clockid_t clockid, struct sigevent *evp, timer_t *timerid)
#else
timer_create(clockid_t clockid, struct sigevent *evp, timer_t *timerid)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timer_create) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || ft_no_clockid(clockid)) {
		return next_timer_create(clockid, evp, timerid);
	}

	int result = next_timer_create(clockid, evp, timerid);

	if (!result && timerid) {
		struct ft_timer_node *node = malloc(sizeof(*node));
		if (!node) {
			assert(next_timer_delete);
			next_timer_delete(*timerid);
			return -1;
		}

		node->timerid = *timerid;
		node->clockid = clockid;

		while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
			;
		node->next = ft_timer_list;
		ft_timer_list = node;
		pthread_mutex_unlock(&ft_timer_mtx);
	}

	return result;
}
#ifdef __GLIBC__
__asm__(".symver timer_create_233, timer_create@@GLIBC_2.3.3");
#endif

int
#ifdef __GLIBC__
timer_delete_233(timer_t timerid)
#else
timer_delete(timer_t timerid)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timer_delete) {
		errno = ENOSYS;
		return -1;
	}

	struct ft_timer_node *node = NULL;
	while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
		;
	for (struct ft_timer_node **pnode = &ft_timer_list; (node = *pnode);
			pnode = &(*pnode)->next) {
		if ((*pnode)->timerid == timerid) {
			*pnode = (*pnode)->next;
			break;
		}
	}
	pthread_mutex_unlock(&ft_timer_mtx);
	free(node);

	return next_timer_delete(timerid);
}
#ifdef __GLIBC__
__asm__(".symver timer_delete_233, timer_delete@@GLIBC_2.3.3");
#endif

int
#ifdef __GLIBC__
timer_gettime_233(timer_t timerid, struct itimerspec *value)
#else
timer_gettime(timer_t timerid, struct itimerspec *value)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	clockid_t clockid = CLOCK_REALTIME;
	if (!next_timer_gettime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !ft_timer_find(timerid, &clockid)
			|| !value) {
		return next_timer_gettime(timerid, value);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clockid);

	ft_fake = false;
	int result = next_timer_gettime(timerid, value);
	ft_fake = true;

	if (!result) {
		value->it_interval = ft_timespec_real2fake_rel(
				&value->it_interval, &attr);
		value->it_value = ft_timespec_real2fake_rel(
				&value->it_value, &attr);
	}

	return result;
}
#ifdef __GLIBC__
__asm__(".symver timer_gettime_233, timer_gettime@@GLIBC_2.3.3");
#endif

int
#ifdef __GLIBC__
timer_settime_233(timer_t timerid, int flags, const struct itimerspec *value,
		struct itimerspec *ovalue)
#else
timer_settime(timer_t timerid, int flags, const struct itimerspec *value,
		struct itimerspec *ovalue)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	clockid_t clockid = CLOCK_REALTIME;
	if (!next_timer_settime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !ft_timer_find(timerid, &clockid)) {
		return next_timer_settime(timerid, flags, value, ovalue);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clockid);

	struct itimerspec next_value = { { 0 }, { 0 } };
	if (value) {
		next_value.it_interval = ft_timespec_fake2real_rel(
				&value->it_interval, &attr);
		// clang-format off
		next_value.it_value = (flags & TIMER_ABSTIME)
				? ft_timespec_fake2real_abs(
						&value->it_value, &attr)
				: ft_timespec_fake2real_rel(
						&value->it_value, &attr);
		// clang-format on
	}

	ft_fake = false;
	int result = next_timer_settime(
			timerid, flags, value ? &next_value : NULL, ovalue);
	ft_fake = true;

	if (!result && ovalue) {
		ovalue->it_interval = ft_timespec_real2fake_rel(
				&ovalue->it_interval, &attr);
		ovalue->it_value = ft_timespec_real2fake_rel(
				&ovalue->it_value, &attr);
	}

	return result;
}
#ifdef __GLIBC__
__asm__(".symver timer_settime_233, timer_settime@@GLIBC_2.3.3");
#endif

#ifdef __GLIBC__

int
timer_create_225(clockid_t clockid, struct sigevent *evp, int *timerid)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timer_create_225) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || ft_no_clockid(clockid)) {
		return next_timer_create_225(clockid, evp, timerid);
	}

	int result = next_timer_create_225(clockid, evp, timerid);

	if (!result && timerid) {
		struct ft_timer_node_225 *node = malloc(sizeof(*node));
		if (!node) {
			assert(next_timer_delete_225);
			next_timer_delete_225(*timerid);
			return -1;
		}

		node->timerid = *timerid;
		node->clockid = clockid;

		while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
			;
		node->next = ft_timer_list_225;
		ft_timer_list_225 = node;
		pthread_mutex_unlock(&ft_timer_mtx);
	}

	return result;
}
__asm__(".symver timer_create_225, timer_create@GLIBC_2.2.5");

int
timer_delete_225(int timerid)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timer_delete_225) {
		errno = ENOSYS;
		return -1;
	}

	struct ft_timer_node_225 *node = NULL;
	while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
		;
	for (struct ft_timer_node_225 **pnode = &ft_timer_list_225;
			(node = *pnode); pnode = &(*pnode)->next) {
		if ((*pnode)->timerid == timerid) {
			*pnode = (*pnode)->next;
			break;
		}
	}
	pthread_mutex_unlock(&ft_timer_mtx);
	free(node);

	return next_timer_delete_225(timerid);
}
__asm__(".symver timer_delete_225, timer_delete@GLIBC_2.2.5");

int
timer_gettime_225(int timerid, struct itimerspec *value)
{
	ft_init();
	FT_TRACE_ALL();
	clockid_t clockid = CLOCK_REALTIME;
	if (!next_timer_gettime_225) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !ft_timer_find_225(timerid, &clockid)
			|| !value) {
		return next_timer_gettime_225(timerid, value);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clockid);

	ft_fake = false;
	int result = next_timer_gettime_225(timerid, value);
	ft_fake = true;

	if (!result) {
		value->it_interval = ft_timespec_real2fake_rel(
				&value->it_interval, &attr);
		value->it_value = ft_timespec_real2fake_rel(
				&value->it_value, &attr);
	}

	return result;
}
__asm__(".symver timer_gettime_225, timer_gettime@GLIBC_2.2.5");

int
timer_settime_225(int timerid, int flags, const struct itimerspec *value,
		struct itimerspec *ovalue)
{
	ft_init();
	FT_TRACE_ALL();
	clockid_t clockid = CLOCK_REALTIME;
	if (!next_timer_settime_225) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake
			|| !ft_timer_find_225(timerid, &clockid)) {
		return next_timer_settime_225(timerid, flags, value, ovalue);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, clockid);

	struct itimerspec next_value = { { 0 }, { 0 } };
	if (value) {
		next_value.it_interval = ft_timespec_fake2real_rel(
				&value->it_interval, &attr);
		// clang-format off
		next_value.it_value = (flags & TIMER_ABSTIME)
				? ft_timespec_fake2real_abs(
						&value->it_value, &attr)
				: ft_timespec_fake2real_rel(
						&value->it_value, &attr);
		// clang-format on
	}

	ft_fake = false;
	int result = next_timer_settime_225(
			timerid, flags, value ? &next_value : NULL, ovalue);
	ft_fake = true;

	if (!result && ovalue) {
		ovalue->it_interval = ft_timespec_real2fake_rel(
				&ovalue->it_interval, &attr);
		ovalue->it_value = ft_timespec_real2fake_rel(
				&ovalue->it_value, &attr);
	}

	return result;
}
__asm__(".symver timer_settime_225, timer_settime@GLIBC_2.2.5");

#endif // __GLIBC__

unsigned
alarm(unsigned seconds)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_alarm && (ft_skip || !ft_fake))
		return next_alarm(seconds);

	struct itimerval value = { { 0, 0 }, { seconds, 0 } };
	struct itimerval ovalue = { { 0 }, { 0 } };

	if (setitimer(ITIMER_REAL, &value, &ovalue))
		return 0;

	unsigned result = 0;
	if (ovalue.it_value.tv_sec > 0) {
		if (ovalue.it_value.tv_sec < (time_t)UINT_MAX) {
			result = ovalue.it_value.tv_sec;
			// Round to the nearest second.
			if (ovalue.it_value.tv_usec >= 500000l)
				result++;
		} else {
			result = UINT_MAX;
		}
	} else if (!ovalue.it_value.tv_sec && ovalue.it_value.tv_usec > 0) {
		// Do not return 0 unless no alarm is set.
		result = 1;
	}

	return result;
}

unsigned
sleep(unsigned seconds)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_sleep && (ft_skip || !ft_fake))
		return next_sleep(seconds);

	struct timespec rqtp = { seconds, 0 };
	struct timespec rmtp = { 0, 0 };

	int errsv = errno;
	int result = nanosleep(&rqtp, &rmtp);
	errno = errsv;

	return result ? rmtp.tv_sec : 0;
}

useconds_t
ualarm(useconds_t useconds, useconds_t interval)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_ualarm && (ft_skip || !ft_fake))
		return next_ualarm(useconds, interval);

	// clang-format off
	struct itimerval value = {
		{ interval / 1000000ul, interval % 1000000ul },
		{ useconds / 1000000ul, useconds % 1000000ul }
	};
	// clang-format on
	struct itimerval ovalue = { { 0 }, { 0 } };

	// POSIX does not specify an error return value, but glibc returns -1.
	if (setitimer(ITIMER_REAL, &value, &ovalue))
		return -1;

	return ovalue.it_value.tv_sec * 1000000ul + ovalue.it_interval.tv_usec;
}

int
usleep(useconds_t useconds)
{
	ft_init();
	FT_TRACE_ALL();
	if (next_usleep && (ft_skip || !ft_fake))
		return next_usleep(useconds);

	struct timespec rqtp = { useconds / 1000000l, useconds % 1000000l };

	return nanosleep(&rqtp, NULL);
}

int
utime(const char *path, const struct utimbuf *times)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_utime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || ft_no_file || !times) {
		return next_utime(path, times);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	// clang-format off
	struct utimbuf next_times = {
		.actime = ft_timespec_fake2real_abs(
				&(struct timespec){ times->actime, 0 },
				&attr).tv_sec,
		.modtime = ft_timespec_fake2real_abs(
				&(struct timespec){ times->modtime, 0 },
				&attr).tv_sec
	};
	// clang-format on

	FT_BLOCK();
	ft_fake = false;
	int result = next_utime(path, &next_times);
	ft_fake = true;
	FT_UNBLOCK();

	return result;
}

#ifdef __GLIBC__

int
gai_suspend(const struct gaicb *const list[], int nitems,
		const struct timespec *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_gai_suspend) {
		errno = ENOSYS;
		return EAI_SYSTEM;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_gai_suspend(list, nitems, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_gai_suspend(list, nitems, &next_timeout);
	ft_fake = true;

	return result;
}

int
semtimedop(int semid, struct sembuf *sops, size_t nsops,
		const struct timespec *timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_semtimedop) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_semtimedop(semid, sops, nsops, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_semtimedop(semid, sops, nsops, &next_timeout);
	ft_fake = true;

	return result;
}

#endif // __GLIBC__

#ifdef __linux__

int
epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_epoll_wait) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || timeout <= 0) {
		return next_epoll_wait(epfd, events, maxevents, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	int next_timeout = ft_timeout_fake2real(timeout, &attr);

	ft_fake = false;
	int result = next_epoll_wait(epfd, events, maxevents, next_timeout);
	ft_fake = true;

	return result;
}

int
epoll_pwait(int epfd, struct epoll_event *events, int maxevents, int timeout,
		const sigset_t *sigmask)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_epoll_pwait) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || timeout <= 0) {
		return next_epoll_pwait(
				epfd, events, maxevents, timeout, sigmask);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	int next_timeout = ft_timeout_fake2real(timeout, &attr);

	ft_fake = false;
	int result = next_epoll_pwait(
			epfd, events, maxevents, next_timeout, sigmask);
	ft_fake = true;

	return result;
}

int
ioctl(int fd, unsigned long request, char *argp)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_ioctl) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || request != SIOCGSTAMP) {
		return next_ioctl(fd, request, argp);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_REALTIME);

	ft_fake = false;
	int result = next_ioctl(fd, request, argp);
	ft_fake = true;

	if (!result) {
		struct timeval *tp = (struct timeval *)argp;
		*tp = ft_timeval_real2fake_abs(tp, &attr);
	}

	return result;
}

int
recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen, int flags,
#if __GLIBC__ == 2 && __GLIBC_MINOR__ < 21
		const struct timespec *timeout)
#else
		struct timespec *timeout)
#endif
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_recvmmsg) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !timeout) {
		return next_recvmmsg(sockfd, msgvec, vlen, flags, timeout);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, CLOCK_MONOTONIC);

	struct timespec next_timeout =
			ft_timespec_fake2real_rel(timeout, &attr);

	ft_fake = false;
	int result = next_recvmmsg(sockfd, msgvec, vlen, flags, &next_timeout);
	ft_fake = true;

	return result;
}

int
timerfd_create(int clockid, int flags)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timerfd_create) {
		errno = ENOSYS;
		return -1;
	}

	int result = next_timerfd_create(clockid, flags);

	if (result >= FT_MAX_FD) {
		close(result);
		errno = EMFILE;
		return -1;
	} else if (result >= 0) {
		ft_timerfd_clockid[result] = clockid;
	}

	return result;
}

int
timerfd_settime(int fd, int flags, const struct itimerspec *new_value,
		struct itimerspec *old_value)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timerfd_settime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || fd < 0 || fd >= FT_MAX_FD) {
		return next_timerfd_settime(fd, flags, new_value, old_value);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, ft_timerfd_clockid[fd]);

	struct itimerspec next_new_value = { { 0 }, { 0 } };
	if (new_value) {
		next_new_value.it_interval = ft_timespec_fake2real_rel(
				&new_value->it_interval, &attr);
		// clang-format off
		next_new_value.it_value = (flags & TFD_TIMER_ABSTIME)
				? ft_timespec_fake2real_abs(
						&new_value->it_value, &attr)
				: ft_timespec_fake2real_rel(
						&new_value->it_value, &attr);
		// clang-format on
	}

	ft_fake = false;
	int result = next_timerfd_settime(fd, flags,
			new_value ? &next_new_value : NULL, old_value);
	ft_fake = true;

	if (!result && old_value) {
		old_value->it_interval = ft_timespec_real2fake_rel(
				&old_value->it_interval, &attr);
		old_value->it_value = ft_timespec_real2fake_rel(
				&old_value->it_value, &attr);
	}

	return result;
}

int
timerfd_gettime(int fd, struct itimerspec *curr_value)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_timerfd_gettime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake || !curr_value || fd < 0
			|| fd >= FT_MAX_FD) {
		return next_timerfd_gettime(fd, curr_value);
	}
	FT_TRACE_FAKE();

	struct ft_clockattr attr;
	ft_clockattr_load(&attr, ft_timerfd_clockid[fd]);

	ft_fake = false;
	int result = timerfd_gettime(fd, curr_value);
	ft_fake = true;

	if (!result) {
		curr_value->it_interval = ft_timespec_real2fake_rel(
				&curr_value->it_interval, &attr);
		curr_value->it_value = ft_timespec_real2fake_rel(
				&curr_value->it_value, &attr);
	}

	return result;
}

int
adjtimex(struct timex *buf)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_adjtimex) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_adjtimex(buf);
	} else {
		// We do not support fake clock adjustments.
		errno = EPERM;
		return -1;
	}
}

int
ntp_adjtime(struct timex *buf)
{
	ft_init();
	FT_TRACE_ALL();
	if (!next_ntp_adjtime) {
		errno = ENOSYS;
		return -1;
	} else if (ft_skip || !ft_fake) {
		return next_ntp_adjtime(buf);
	} else {
		// We do not support fake clock adjustments.
		errno = EPERM;
		return -1;
	}
}

#endif // __linux__

static void
ft_init(void)
{
	static pthread_once_t once_control = PTHREAD_ONCE_INIT;
	pthread_once(&once_control, &ft_init_routine);
}

#ifdef __GNUC__
static void
ft_fini(void)
{
	if (ft_shm) {
		munmap(ft_shm, sizeof(*ft_shm));
		ft_shm = NULL;
	}
}
#endif

static void
ft_init_routine(void)
{
	int errsv = errno;
	ft_fake = false;

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif

	next_aio_suspend = dlsym(RTLD_NEXT, "aio_suspend");
#ifdef __GLIBC__
	next_aio_suspend64 = dlsym(RTLD_NEXT, "aio_suspend64");
#endif

	next_utimensat = dlsym(RTLD_NEXT, "utimensat");

	next_mq_timedreceive = dlsym(RTLD_NEXT, "mq_timedreceive");
	next_mq_timedsend = dlsym(RTLD_NEXT, "mq_timedsend");

	next_poll = dlsym(RTLD_NEXT, "poll");
#ifdef __linux__
	next_ppoll = dlsym(RTLD_NEXT, "ppoll");
#endif

#ifdef __GLIBC__
	next_pthread_cond_destroy = dlvsym(
			RTLD_NEXT, "pthread_cond_destroy", "GLIBC_2.3.2");
	if (!next_pthread_cond_destroy)
		next_pthread_cond_destroy =
				dlsym(RTLD_NEXT, "pthread_cond_destroy");
	next_pthread_cond_init =
			dlvsym(RTLD_NEXT, "pthread_cond_init", "GLIBC_2.3.2");
	if (!next_pthread_cond_init)
		next_pthread_cond_init = dlsym(RTLD_NEXT, "pthread_cond_init");
	next_pthread_cond_timedwait = dlvsym(
			RTLD_NEXT, "pthread_cond_timedwait", "GLIBC_2.3.2");
	if (!next_pthread_cond_timedwait)
		next_pthread_cond_timedwait =
				dlsym(RTLD_NEXT, "pthread_cond_timedwait");
	next_pthread_cond_timedwait_225 = dlvsym(
			RTLD_NEXT, "pthread_cond_timedwait", "GLIBC_2.2.5");
#else
	next_pthread_cond_destroy = dlsym(RTLD_NEXT, "pthread_cond_destroy");
	next_pthread_cond_init = dlsym(RTLD_NEXT, "pthread_cond_init");
	next_pthread_cond_timedwait =
			dlsym(RTLD_NEXT, "pthread_cond_timedwait");
#endif
	next_pthread_mutex_timedlock =
			dlsym(RTLD_NEXT, "pthread_mutex_timedlock");
	next_pthread_rwlock_timedrdlock =
			dlsym(RTLD_NEXT, "pthread_rwlock_timedrdlock");
	next_pthread_rwlock_timedwrlock =
			dlsym(RTLD_NEXT, "pthread_rwlock_timedwrlock");
#ifdef __GLIBC__
	next_pthread_timedjoin_np = dlsym(RTLD_NEXT, "pthread_timedjoin_np");
#endif

	// next_sched_rr_get_interval = dlsym(RTLD_NEXT,
	// "sched_rr_get_interval");

	next_sem_timedwait = dlsym(RTLD_NEXT, "sem_timedwait");

	next_sigtimedwait = dlsym(RTLD_NEXT, "sigtimedwait");

	next_pselect = dlsym(RTLD_NEXT, "pselect");
	next_select = dlsym(RTLD_NEXT, "select");

	next_getsockopt = dlsym(RTLD_NEXT, "getsockopt");
	next_setsockopt = dlsym(RTLD_NEXT, "setsockopt");

#ifdef __GLIBC__
	next_fstat = dlsym(RTLD_NEXT, "__fxstat");
	next_fstatat = dlsym(RTLD_NEXT, "__fxstatat");
#else
	next_fstat = dlsym(RTLD_NEXT, "fstat");
	next_fstatat = dlsym(RTLD_NEXT, "fstatat");
#endif
	next_futimens = dlsym(RTLD_NEXT, "futimens");
#ifdef __GLIBC__
	next_lstat = dlsym(RTLD_NEXT, "__lxstat");
	next_stat = dlsym(RTLD_NEXT, "__xstat");
	next_fstat64 = dlsym(RTLD_NEXT, "__fxstat64");
	next_fstatat64 = dlsym(RTLD_NEXT, "__fxstatat64");
	next_lstat64 = dlsym(RTLD_NEXT, "__lxstat64");
	next_stat64 = dlsym(RTLD_NEXT, "__xstat64");
#else
	next_lstat = dlsym(RTLD_NEXT, "lstat");
	next_stat = dlsym(RTLD_NEXT, "stat");
#endif

	next_getitimer = dlsym(RTLD_NEXT, "getitimer");
	next_gettimeofday = dlsym(RTLD_NEXT, "gettimeofday");
	next_setitimer = dlsym(RTLD_NEXT, "setitimer");
#ifdef __GLIBC__
	next_settimeofday = dlsym(RTLD_NEXT, "settimeofday");
	next_adjtime = dlsym(RTLD_NEXT, "adjtime");
	next_futimes = dlsym(RTLD_NEXT, "futimes");
	next_lutimes = dlsym(RTLD_NEXT, "lutimes");
	next_futimesat = dlsym(RTLD_NEXT, "futimesat");
#endif
	next_utimes = dlsym(RTLD_NEXT, "utimes");

	next_ftime = dlsym(RTLD_NEXT, "ftime");

	// next_times = dlsym(RTLD_NEXT, "times");

	// next_clock = dlsym(RTLD_NEXT, "clock");
	next_clock_getres = dlsym(RTLD_NEXT, "clock_getres");
	next_clock_gettime = dlsym(RTLD_NEXT, "clock_gettime");
	next_clock_nanosleep = dlsym(RTLD_NEXT, "clock_nanosleep");
	next_clock_settime = dlsym(RTLD_NEXT, "clock_settime");
	next_nanosleep = dlsym(RTLD_NEXT, "nanosleep");
	next_time = dlsym(RTLD_NEXT, "time");
#if __STDC_VERSION__ >= 201112L
	next_timespec_get = dlsym(RTLD_NEXT, "timespec_get");
#endif
#ifdef __GLIBC__
	next_timer_create = dlvsym(RTLD_NEXT, "timer_create", "GLIBC_2.3.3");
	if (!next_timer_create)
		next_timer_create = dlsym(RTLD_NEXT, "timer_create");
	next_timer_delete = dlvsym(RTLD_NEXT, "timer_delete", "GLIBC_2.3.3");
	if (!next_timer_delete)
		next_timer_delete = dlsym(RTLD_NEXT, "timer_delete");
	next_timer_gettime = dlvsym(RTLD_NEXT, "timer_gettime", "GLIBC_2.3.3");
	if (!next_timer_gettime)
		next_timer_gettime = dlsym(RTLD_NEXT, "timer_gettime");
	next_timer_settime = dlvsym(RTLD_NEXT, "timer_settime", "GLIBC_2.3.3");
	if (!next_timer_settime)
		next_timer_settime = dlsym(RTLD_NEXT, "timer_settime");
	next_timer_create_225 =
			dlvsym(RTLD_NEXT, "timer_create", "GLIBC_2.2.5");
	next_timer_delete_225 =
			dlvsym(RTLD_NEXT, "timer_delete", "GLIBC_2.2.5");
	next_timer_gettime_225 =
			dlvsym(RTLD_NEXT, "timer_gettime", "GLIBC_2.2.5");
	next_timer_settime_225 =
			dlvsym(RTLD_NEXT, "timer_settime", "GLIBC_2.2.5");
#else
	next_timer_create = dlsym(RTLD_NEXT, "timer_create");
	next_timer_delete = dlsym(RTLD_NEXT, "timer_delete");
	next_timer_gettime = dlsym(RTLD_NEXT, "timer_gettime");
	next_timer_settime = dlsym(RTLD_NEXT, "timer_settime");
#endif

	next_alarm = dlsym(RTLD_NEXT, "alarm");
	next_sleep = dlsym(RTLD_NEXT, "sleep");
	next_ualarm = dlsym(RTLD_NEXT, "ualarm");
	next_usleep = dlsym(RTLD_NEXT, "usleep");

	next_utime = dlsym(RTLD_NEXT, "utime");

#ifdef __GLIBC__
	next_gai_suspend = dlsym(RTLD_NEXT, "gai_suspend");

	next_semtimedop = dlsym(RTLD_NEXT, "semtimedop");
#endif

#ifdef __linux__
	next_epoll_wait = dlsym(RTLD_NEXT, "epoll_wait");
	next_epoll_pwait = dlsym(RTLD_NEXT, "epoll_pwait");

	next_ioctl = dlsym(RTLD_NEXT, "ioctl");

	next_recvmmsg = dlsym(RTLD_NEXT, "recvmmsg");

	next_timerfd_create = dlsym(RTLD_NEXT, "timerfd_create");
	next_timerfd_settime = dlsym(RTLD_NEXT, "timerfd_settime");
	next_timerfd_gettime = dlsym(RTLD_NEXT, "timerfd_gettime");

	next_adjtimex = dlsym(RTLD_NEXT, "adjtimex");
	next_ntp_adjtime = dlsym(RTLD_NEXT, "ntp_adjtime");
	// next_ntp_gettime = dlsym(RTLD_NEXT, "ntp_gettime");
	// next_ntp_gettimex = dlsym(RTLD_NEXT, "ntp_gettimex");
#endif // __linux

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

	char *envval;

#ifdef __GLIBC__
	const char *progname = program_invocation_name;
#else
	const char *progname = NULL;
#endif
	if (progname) {
		const char *name = progname;
		while (*name)
			name++;
		while (name >= progname && *name != '/')
			name--;
		name++;

		if ((envval = getenv("FT_ONLY"))) {
			ft_skip = true;
		} else if ((envval = getenv("FT_SKIP"))) {
			ft_skip = false;
		}
		if (envval) {
			char *state = NULL;
			char *tok;
			while ((tok = strtok_r(envval, ",", &state))) {
				envval = NULL;
				if (!strcmp(tok, name)) {
					ft_skip = !ft_skip;
					break;
				}
			}
		}
	}

	if ((envval = getenv("FT_NO"))) {
		char *state = NULL;
		char *tok;
		while ((tok = strtok_r(envval, ",", &state))) {
			envval = NULL;
			if (!strcasecmp(tok, "realtime")) {
				ft_no_realtime = true;
			} else if (!strcasecmp(tok, "monotonic")) {
				ft_no_monotonic = true;
#ifdef CLOCK_MONOTONIC_RAW
			} else if (!strcasecmp(tok, "monotonic_raw")) {
				ft_no_monotonic_raw = true;
#endif
#ifdef CLOCK_REALTIME_COARSE
			} else if (!strcasecmp(tok, "realtime_coarse")) {
				ft_no_realtime_coarse = true;
#endif
#ifdef CLOCK_MONOTONIC_COARSE
			} else if (!strcasecmp(tok, "monotonic_coarse")) {
				ft_no_monotonic_coarse = true;
#endif
#ifdef CLOCK_BOOTTIME
			} else if (!strcasecmp(tok, "boottime")) {
				ft_no_boottime = true;
#endif
#ifdef CLOCK_TAI
			} else if (!strcasecmp(tok, "tai")) {
				ft_no_tai = true;
#endif
			} else if (!strcasecmp(tok, "file")) {
				ft_no_file = true;
			} else if (!strcasecmp(tok, "settime")) {
				ft_no_settime = true;
			}
		}
	}

	if ((envval = getenv("FT_TRACE"))) {
		if (!strcasecmp(envval, "all")) {
			ft_trace = TRACE_ALL;
		} else if (!strcasecmp(envval, "fake")) {
			ft_trace = TRACE_FAKE;
		}
	}

	if ((envval = getenv("FT_SHM")) && *envval)
		ft_shm = ft_shm_open(envval);
	if (ft_shm) {
		ft_info = &ft_shm->info;
	} else {
		static struct ft_info info;
		ft_info_init(&info);
		ft_info = &info;
	}

	if (!ft_no_realtime) {
		setenvrate("FT_REALTIME_RATE", ft_info->realtime.rate, 1);
		setenvts("FT_REALTIME_BEGIN", &ft_info->realtime.begin, 1);
		setenvts("FT_REALTIME_OFFSET", &ft_info->realtime.offset, 1);
	}

	if (!ft_no_monotonic) {
		setenvrate("FT_MONOTONIC_RATE", ft_info->monotonic.rate, 1);
		setenvts("FT_MONOTONIC_BEGIN", &ft_info->monotonic.begin, 1);
		setenvts("FT_MONOTONIC_OFFSET", &ft_info->monotonic.offset, 1);
	}

#ifdef CLOCK_MONOTONIC_RAW
	if (!ft_no_monotonic_raw) {
		setenvrate("FT_MONOTONIC_RAW_RATE", ft_info->monotonic_raw.rate,
				1);
		setenvts("FT_MONOTONIC_RAW_BEGIN",
				&ft_info->monotonic_raw.begin, 1);
		setenvts("FT_MONOTONIC_RAW_OFFSET",
				&ft_info->monotonic_raw.offset, 1);
	}
#endif

#ifdef CLOCK_REALTIME_COARSE
	if (!ft_no_realtime_coarse) {
		setenvrate("FT_REALTIME_COARSE_RATE",
				ft_info->realtime_coarse.rate, 1);
		setenvts("FT_REALTIME_COARSE_BEGIN",
				&ft_info->realtime_coarse.begin, 1);
		setenvts("FT_REALTIME_COARSE_OFFSET",
				&ft_info->realtime_coarse.offset, 1);
	}
#endif

#ifdef CLOCK_MONOTONIC_COARSE
	if (!ft_no_monotonic_coarse) {
		setenvrate("FT_MONOTONIC_COARSE_RATE",
				ft_info->monotonic_coarse.rate, 1);
		setenvts("FT_MONOTONIC_COARSE_BEGIN",
				&ft_info->monotonic_coarse.begin, 1);
		setenvts("FT_MONOTONIC_COARSE_OFFSET",
				&ft_info->monotonic_coarse.offset, 1);
	}
#endif

#ifdef CLOCK_BOOTTIME
	if (!ft_no_boottime) {
		setenvrate("FT_BOOTTIME_RATE", ft_info->boottime.rate, 1);
		setenvts("FT_BOOTTIME_BEGIN", &ft_info->boottime.begin, 1);
		setenvts("FT_BOOTTIME_OFFSET", &ft_info->boottime.offset, 1);
	}
#endif

#ifdef CLOCK_TAI
	if (!ft_no_tai) {
		setenvrate("FT_TAI_RATE", ft_info->tai.rate, 1);
		setenvts("FT_TAI_BEGIN", &ft_info->tai.begin, 1);
		setenvts("FT_TAI_OFFSET", &ft_info->tai.offset, 1);
	}
#endif

	// Only try to be async-signal-safe when not using shared memory.
	ft_safe = !ft_shm;
	sigfillset(&ft_set);

	ft_fake = true;
	errno = errsv;
}

static bool
ft_no_clockid(clockid_t clockid)
{
	switch (clockid) {
	case CLOCK_REALTIME:
#ifdef CLOCK_REALTIME_ALARM
	case CLOCK_REALTIME_ALARM:
#endif
		return ft_no_realtime;
	case CLOCK_MONOTONIC: return ft_no_monotonic;
	case CLOCK_PROCESS_CPUTIME_ID:
	case CLOCK_THREAD_CPUTIME_ID: return true;
#ifdef CLOCK_MONOTONIC_RAW
	case CLOCK_MONOTONIC_RAW: return ft_no_monotonic_raw;
#endif
#ifdef CLOCK_REALTIME_COARSE
	case CLOCK_REALTIME_COARSE: return ft_no_realtime_coarse;
#endif
#ifdef CLOCK_MONOTONIC_COARSE
	case CLOCK_MONOTONIC_COARSE: return ft_no_monotonic_coarse;
#endif
#ifdef CLOCK_BOOTTIME
	case CLOCK_BOOTTIME:
#ifdef CLOCK_BOOTTIME_ALARM
	case CLOCK_BOOTTIME_ALARM:
#endif
		return ft_no_boottime;
#endif
#ifdef CLOCK_TAI
	case CLOCK_TAI: return ft_no_tai;
#endif
	default: return false;
	}
}

static void
ft_print_trace(const char *func)
{
	fflush(stdout);
	fprintf(stderr, "%s()\n", func);
}

static void
ft_clockattr_init(struct ft_clockattr *attr, clockid_t clockid, double rate,
		const struct timespec *offset)
{
	assert(attr);
	assert(offset);

	struct timespec now = { 0, 0 };
	if (next_clock_gettime)
		next_clock_gettime(clockid, &now);

	*attr = (struct ft_clockattr){ rate, now, *offset };
}

static void
ft_clockattr_load(struct ft_clockattr *attr, clockid_t clockid)
{
	assert(attr);

	switch (clockid) {
	case CLOCK_REALTIME:
#ifdef CLOCK_REALTIME_ALARM
	case CLOCK_REALTIME_ALARM:
#endif
		if (ft_no_realtime)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->realtime;
		ft_shm_unlock(ft_shm);
		return;
	case CLOCK_MONOTONIC:
		if (ft_no_monotonic)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->monotonic;
		ft_shm_unlock(ft_shm);
		return;
#ifdef CLOCK_MONOTONIC_RAW
	case CLOCK_MONOTONIC_RAW:
		if (ft_no_monotonic_raw)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->monotonic_raw;
		ft_shm_unlock(ft_shm);
		return;
#endif
#ifdef CLOCK_REALTIME_COARSE
	case CLOCK_REALTIME_COARSE:
		if (ft_no_realtime_coarse)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->realtime_coarse;
		ft_shm_unlock(ft_shm);
		return;
#endif
#ifdef CLOCK_MONOTONIC_COARSE
	case CLOCK_MONOTONIC_COARSE:
		if (ft_no_monotonic_coarse)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->monotonic_coarse;
		ft_shm_unlock(ft_shm);
		return;
#endif
#ifdef CLOCK_BOOTTIME
	case CLOCK_BOOTTIME:
#ifdef CLOCK_BOOTTIME_ALARM
	case CLOCK_BOOTTIME_ALARM:
#endif
		if (ft_no_boottime)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->boottime;
		ft_shm_unlock(ft_shm);
		return;
#endif
#ifdef CLOCK_TAI
	case CLOCK_TAI:
		if (ft_no_tai)
			break;
		ft_shm_lock(ft_shm);
		*attr = ft_info->tai;
		ft_shm_unlock(ft_shm);
		return;
#endif
	}
	*attr = (struct ft_clockattr)FT_CLOCKATTR_INIT;
}

static bool
ft_timer_find(timer_t timerid, clockid_t *pclockid)
{
	assert(pclockid);

	while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
		;
	for (struct ft_timer_node *node = ft_timer_list; node;
			node = node->next) {
		if (node->timerid == timerid) {
			*pclockid = node->clockid;
			pthread_mutex_unlock(&ft_timer_mtx);
			return true;
		}
	}
	pthread_mutex_unlock(&ft_timer_mtx);
	return false;
}

#ifdef __GLIBC__
static bool
ft_timer_find_225(int timerid, clockid_t *pclockid)
{
	assert(pclockid);

	while (pthread_mutex_lock(&ft_timer_mtx) == EINTR)
		;
	for (struct ft_timer_node_225 *node = ft_timer_list_225; node;
			node = node->next) {
		if (node->timerid == timerid) {
			*pclockid = node->clockid;
			pthread_mutex_unlock(&ft_timer_mtx);
			return true;
		}
	}
	pthread_mutex_unlock(&ft_timer_mtx);
	return false;
}
#endif // __GLIBC__

static int
ft_timeout_fake2real(int timeout, const struct ft_clockattr *attr)
{
	assert(attr);

	if (timeout <= 0)
		return timeout;

	struct timespec ts = { timeout / 1000, (timeout % 1000) * 1000000l };
	ts = ft_timespec_fake2real_rel(&ts, attr);
	if (ts.tv_sec >= (INT_MAX - ts.tv_nsec / 1000000l) / 1000)
		return INT_MAX;
	timeout = ts.tv_sec * 1000 + ts.tv_nsec / 1000000l;
	return timeout ? timeout : 1;
}

static struct timespec
ft_timespec_fake2real_utime(
		const struct timespec *tp, const struct ft_clockattr *attr)
{
	assert(tp);

	return tp->tv_nsec == UTIME_NOW || tp->tv_nsec == UTIME_OMIT
			? *tp
			: ft_timespec_fake2real_abs(tp, attr);
}

static struct timespec
ft_timespec_fake2real_abs(
		const struct timespec *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	struct timespec ts = timespec_sub(tp, &attr->offset);
	ts = timespec_sub(&ts, &attr->begin);
	ts = ft_timespec_fake2real_rel(&ts, attr);
	return timespec_add(&ts, &attr->begin);
}

static struct timespec
ft_timespec_real2fake_abs(
		const struct timespec *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	struct timespec ts = timespec_sub(tp, &attr->begin);
	ts = ft_timespec_real2fake_rel(&ts, attr);
	ts = timespec_add(&ts, &attr->begin);
	return timespec_add(&ts, &attr->offset);
}

static struct timespec
ft_timespec_fake2real_rel(
		const struct timespec *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	if (!tp->tv_sec && !tp->tv_nsec)
		return *tp;

	fenv_t env;
	if (!ft_safe)
		feholdexcept(&env);
	struct timespec ts = timespec_div(tp, attr->rate);
	if (!ft_safe)
		fesetenv(&env);

	return ts;
}

static struct timespec
ft_timespec_real2fake_rel(
		const struct timespec *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	if (!tp->tv_sec && !tp->tv_nsec)
		return *tp;

	fenv_t env;
	if (!ft_safe)
		feholdexcept(&env);
	struct timespec ts = timespec_mul(tp, attr->rate);
	if (!ft_safe)
		fesetenv(&env);

	return ts;
}

static struct timeval
ft_timeval_fake2real_abs(
		const struct timeval *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	struct timeval begin = timespec2val(&attr->begin);
	struct timeval offset = timespec2val(&attr->offset);

	struct timeval tv = timeval_sub(tp, &offset);
	tv = timeval_sub(&tv, &begin);
	tv = ft_timeval_fake2real_rel(&tv, attr);
	return timeval_add(&tv, &begin);
}

static struct timeval
ft_timeval_real2fake_abs(
		const struct timeval *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	struct timeval begin = timespec2val(&attr->begin);
	struct timeval offset = timespec2val(&attr->offset);

	struct timeval tv = timeval_sub(tp, &begin);
	tv = ft_timeval_real2fake_rel(&tv, attr);
	tv = timeval_add(&tv, &begin);
	return timeval_add(&tv, &offset);
}

static struct timeval
ft_timeval_fake2real_rel(
		const struct timeval *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	if (!tp->tv_sec && !tp->tv_usec)
		return *tp;

	fenv_t env;
	if (!ft_safe)
		feholdexcept(&env);
	struct timeval tv = timeval_div(tp, attr->rate);
	if (!ft_safe)
		fesetenv(&env);

	return tv;
}

static struct timeval
ft_timeval_real2fake_rel(
		const struct timeval *tp, const struct ft_clockattr *attr)
{
	assert(tp);
	assert(attr);

	if (!tp->tv_sec && !tp->tv_usec)
		return *tp;

	fenv_t env;
	if (!ft_safe)
		feholdexcept(&env);
	struct timeval tv = timeval_mul(tp, attr->rate);
	if (!ft_safe)
		fesetenv(&env);

	return tv;
}

static struct timeval
timespec2val(const struct timespec *tp)
{
	assert(tp);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);

	return (struct timeval){ tp->tv_sec, tp->tv_nsec / 1000 };
}

static struct timespec
timespec_sub(const struct timespec *tp, const struct timespec *dec)
{
	assert(tp);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);
	assert(dec);
	assert(dec->tv_nsec >= 0);
	assert(dec->tv_nsec < 1000000000l);

	uintmax_t tv_sec = (uintmax_t)tp->tv_sec - (uintmax_t)dec->tv_sec;
	long tv_nsec = tp->tv_nsec - dec->tv_nsec;
	if (tv_nsec < 0) {
		tv_sec--;
		tv_nsec += 1000000000l;
	}
	tv_sec &= UTIME_T_MAX;
	return (struct timespec){ tv_sec <= (uintmax_t)TIME_T_MAX
				? (time_t)tv_sec
				: -(time_t)((UTIME_T_MAX - tv_sec) + 1),
		tv_nsec };
}

static struct timespec
timespec_mul(const struct timespec *tp, double mul)
{
	assert(tp);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);
	assert(isfinite(mul));

	if ((!tp->tv_sec && !tp->tv_nsec) || mul == 1.)
		return *tp;

	double nsec = ((double)tp->tv_sec * 1000000000l + tp->tv_nsec) * mul;
	if (isfinite(nsec) && nsec > INTMAX_MIN && nsec < INTMAX_MAX) {
		long tv_nsec = (intmax_t)nsec % 1000000000l;
		intmax_t tv_sec = ((intmax_t)nsec - tv_nsec) / 1000000000l;
		if (tv_nsec < 0) {
			tv_sec--;
			tv_nsec += 1000000000l;
		}
		if (tv_sec > TIME_T_MIN && tv_sec < TIME_T_MAX) {
			if (tv_sec || tv_nsec)
				return (struct timespec){ tv_sec, tv_nsec };
			if ((tp->tv_sec < 0) ^ signbit(mul))
				return (struct timespec){ -1, 999999999l };
			return (struct timespec){ 0, 1 };
		}
	}

	return (tp->tv_sec < 0) ^ signbit(mul) ? TIMESPEC_MIN : TIMESPEC_MAX;
}

static struct timespec
timespec_div(const struct timespec *tp, double div)
{
	assert(tp);
	assert(tp->tv_sec || tp->tv_nsec);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);
	assert(isfinite(div));

	if ((!tp->tv_sec && !tp->tv_nsec) || div == 1.)
		return *tp;

	double nsec = ((double)tp->tv_sec * 1000000000l + tp->tv_nsec) / div;
	if (isfinite(nsec) && nsec > INTMAX_MIN && nsec < INTMAX_MAX) {
		long tv_nsec = (intmax_t)nsec % 1000000000l;
		intmax_t tv_sec = ((intmax_t)nsec - tv_nsec) / 1000000000l;
		if (tv_nsec < 0) {
			tv_sec--;
			tv_nsec += 1000000000l;
		}
		if (tv_sec > TIME_T_MIN && tv_sec < TIME_T_MAX) {
			if (tv_sec || tv_nsec)
				return (struct timespec){ tv_sec, tv_nsec };
			if ((tp->tv_sec < 0) ^ signbit(div))
				return (struct timespec){ -1, 999999999l };
			return (struct timespec){ 0, 1 };
		}
	}

	return (tp->tv_sec < 0) ^ signbit(div) ? TIMESPEC_MIN : TIMESPEC_MAX;
}

static struct timeval
timeval_add(const struct timeval *tp, const struct timeval *inc)
{
	assert(tp);
	assert(tp->tv_usec >= 0);
	assert(tp->tv_usec < 1000000l);
	assert(inc);
	assert(inc->tv_usec >= 0);
	assert(inc->tv_usec < 1000000l);

	uintmax_t tv_sec = (uintmax_t)tp->tv_sec + (uintmax_t)inc->tv_sec;
	long tv_usec = tp->tv_usec + inc->tv_usec;
	if (tv_usec >= 1000000l) {
		tv_sec++;
		tv_usec -= 1000000l;
	}
	tv_sec &= UTIME_T_MAX;
	return (struct timeval){ tv_sec <= (uintmax_t)TIME_T_MAX
				? (time_t)tv_sec
				: -(time_t)((UTIME_T_MAX - tv_sec) + 1),
		tv_usec };
}

static struct timeval
timeval_sub(const struct timeval *tp, const struct timeval *dec)
{
	assert(tp);
	assert(tp->tv_usec >= 0);
	assert(tp->tv_usec < 1000000l);
	assert(dec);
	assert(dec->tv_usec >= 0);
	assert(dec->tv_usec < 1000000l);

	uintmax_t tv_sec = (uintmax_t)tp->tv_sec - (uintmax_t)dec->tv_sec;
	long tv_usec = tp->tv_usec - dec->tv_usec;
	if (tv_usec < 0) {
		tv_sec--;
		tv_usec += 1000000l;
	}
	tv_sec &= UTIME_T_MAX;
	return (struct timeval){ tv_sec <= (uintmax_t)TIME_T_MAX
				? (time_t)tv_sec
				: -(time_t)((UTIME_T_MAX - tv_sec) + 1),
		tv_usec };
}

static struct timeval
timeval_mul(const struct timeval *tp, double mul)
{
	assert(tp);
	assert(tp->tv_usec >= 0);
	assert(tp->tv_usec < 1000000l);
	assert(isfinite(mul));

	if ((!tp->tv_sec && !tp->tv_usec) || mul == 1.)
		return *tp;

	double usec = ((double)tp->tv_sec * 1000000l + tp->tv_usec) * mul;
	if (isfinite(usec) && usec > INTMAX_MIN && usec < INTMAX_MAX) {
		long tv_usec = (intmax_t)usec % 1000000l;
		intmax_t tv_sec = ((intmax_t)usec - tv_usec) / 1000000l;
		if (tv_usec < 0) {
			tv_sec--;
			tv_usec += 1000000l;
		}
		if (tv_sec > TIME_T_MIN && tv_sec < TIME_T_MAX) {
			if (tv_sec || tv_usec)
				return (struct timeval){ tv_sec, tv_usec };
			if ((tp->tv_sec < 0) ^ signbit(mul))
				return (struct timeval){ -1, 999999l };
			return (struct timeval){ 0, 1 };
		}
	}

	return (tp->tv_sec < 0) ^ signbit(mul) ? TIMEVAL_MIN : TIMEVAL_MAX;
}

static struct timeval
timeval_div(const struct timeval *tp, double div)
{
	assert(tp);
	assert(tp->tv_usec >= 0);
	assert(tp->tv_usec < 1000000l);
	assert(isfinite(div));

	if ((!tp->tv_sec && !tp->tv_usec) || div == 1.)
		return *tp;

	double usec = ((double)tp->tv_sec * 1000000l + tp->tv_usec) / div;
	if (isfinite(usec) && usec > INTMAX_MIN && usec < INTMAX_MAX) {
		long tv_usec = (intmax_t)usec % 1000000l;
		intmax_t tv_sec = ((intmax_t)usec - tv_usec) / 1000000l;
		if (tv_usec < 0) {
			tv_sec--;
			tv_usec += 1000000l;
		}
		if (tv_sec > TIME_T_MIN && tv_sec < TIME_T_MAX) {
			if (tv_sec || tv_usec)
				return (struct timeval){ tv_sec, tv_usec };
			if ((tp->tv_sec < 0) ^ signbit(div))
				return (struct timeval){ -1, 999999l };
			return (struct timeval){ 0, 1 };
		}
	}

	return (tp->tv_sec < 0) ^ signbit(div) ? TIMEVAL_MIN : TIMEVAL_MAX;
}

#endif // !FT_LIBFT_C_

// SPDX-License-Identifier: Apache-2.0

#include "ft.h"

#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>

#ifndef CMD
#define CMD "ft-exec"
#endif

#define HELP \
	"Usage: " CMD " [OPTION]... COMMAND [ARGUMENT]...\n" \
	"Run COMMAND with fake system-wide real time clocks.\n\n" \
	"Options:\n" \
	"  -h, --help            show this information\n" \
	"  -n  --no=LIST         do not fake the (comma-separated) clocks or function\n" \
	"                        classes in LIST\n" \
	"    Valid clocks in LIST:\n" \
	"      realtime          CLOCK_REALTIME (and CLOCK_REALTIME_ALARM on Linux)\n" \
	"      monotonic         CLOCK_MONOTONIC\n" \
	"      monotonic_raw     CLOCK_MONOTONIC_RAW (on Linux)\n" \
	"      realtime_coarse   CLOCK_REALTIME_COARSE (on Linux)\n" \
	"      monotonic_coarse  CLOCK_MONOTONIC_COARSE (on Linux)\n" \
	"      boottime          CLOCK_BOOTTIME and CLOCK_BOOTTIME_ALARM (on Linux)\n" \
	"      tai               CLOCK_TAI (on Linux)\n" \
	"    Valid function classes in LIST:\n" \
	"      file              function which get or set file modification times:\n" \
	"                        *stat*(), futimens(), utime(), utimensat(), futimes(),\n" \
	"                        lutimes(), futimesat() and utimes()\n" \
	"      settime           function which set the clock time: clock_settime() and\n" \
	"                        settimeofday()\n" \
	"  -o, --offset=OFFSET   add OFFSET to all wall clocks\n" \
	"  -r, --rate=RATE       speed up all wall clocks by a factor RATE\n" \
	"  -t, --trace=WHICH     print trace information for the specified function calls\n" \
	"    WHICH is one of the following:\n" \
	"      all               all intercepted function calls\n" \
	"      fake              only those function calls that fake the clock"

#ifndef LIBFT
#define LIBFT "libft.so"
#endif

static int set_ld_preload(void);

static struct ft_shm *ft_shm_create(const char *name);
static void ft_shm_destroy(struct ft_shm *shm, const char *name);

int
main(int argc, char *argv[])
{
	opterr = 0;
	int errflg = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-')
			break;
		if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "help")) {
				printf("%s\n", HELP);
				return EXIT_SUCCESS;
			} else if (!strncmp(arg, "no=", 3)) {
				setenv("FT_NO", arg + 3, 1);
			} else if (!strncmp(arg, "offset=", 7)) {
				setenv("FT_OFFSET", arg + 7, 1);
			} else if (!strncmp(arg, "rate=", 5)) {
				setenv("FT_RATE", arg + 5, 1);
			} else if (!strncmp(arg, "trace=", 6)) {
				setenv("FT_TRACE", arg + 6, 1);
			} else {
				fprintf(stderr, CMD ": illegal option -- %s\n",
						arg);
				errflg = 1;
			}
		} else {
			int c = getopt(argc, argv, "hn:o:r:t:");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr,
						CMD
						": option requires an argument -- %c\n",
						optopt);
				errflg = 1;
				break;
			case '?':
				fprintf(stderr, CMD ": illegal option -- %c\n",
						optopt);
				errflg = 1;
				break;
			case 'h': printf("%s\n", HELP); return EXIT_SUCCESS;
			case 'n': setenv("FT_NO", optarg, 1); break;
			case 'o': setenv("FT_OFFSET", optarg, 1); break;
			case 'r': setenv("FT_RATE", optarg, 1); break;
			case 't': setenv("FT_TRACE", optarg, 1); break;
			}
		}
	}

	if (optind >= argc) {
		fprintf(stderr, CMD ": no command specified\n");
		errflg = 1;
	}

	if (errflg) {
		fprintf(stderr, "%s\n", HELP);
		return EXIT_FAILURE;
	}

	if (argc <= 1) {
		fprintf(stderr, "%s\n", HELP);
		return EXIT_FAILURE;
	}

	if (set_ld_preload() == -1) {
		perror(CMD ": error: unable to set LD_PRELOAD");
		return EXIT_FAILURE;
	}

	if (setpgid(0, 0) == -1) {
		perror(CMD ": error: unable to set process group ID");
		return EXIT_FAILURE;
	}

	char envval[32] = { 0 };

	snprintf(envval, sizeof(envval), "%ld", (long)getpgid(0));
	if (setenv("FT_PGID", envval, 1) == -1) {
		perror(CMD ": error: unable to set FT_PGID");
		return EXIT_FAILURE;
	}

	snprintf(envval, sizeof(envval), "/ft_shm.%ld", (long)getpid());
	if (setenv("FT_SHM", envval, 0) == -1) {
		perror(CMD ": error: unable to set FT_SHM");
		return EXIT_FAILURE;
	}

	const char *shm_name = getenv("FT_SHM");
	struct ft_shm *shm = ft_shm_create(shm_name);
	if (!shm) {
		perror(shm_name);
		return EXIT_FAILURE;
	}

	ft_info_init(&shm->info);

	pid_t pid = fork();
	if (pid == -1) {
		perror(CMD ": error: unable to fork");
		ft_shm_destroy(shm, shm_name);
		return EXIT_FAILURE;
	}

	if (pid) {
		int stat_info = 0;
		pid_t result;
		while ((result = waitpid(pid, &stat_info, 0)) == -1
				&& errno == EINTR)
			;
		int errsv = errno;
		ft_shm_destroy(shm, shm_name);
		errno = errsv;
		if (result == -1)
			perror(CMD ": error: no child process");
		if (WIFSIGNALED(stat_info)) {
			fprintf(stderr, CMD ": caught %s\n",
					strsignal(WTERMSIG(stat_info)));
			exit(EXIT_FAILURE);
		}
		return WEXITSTATUS(stat_info);
	} else {
		if (execvp(argv[optind], &argv[optind]) == -1)
			perror(argv[0]);
		return EXIT_FAILURE;
	}
}

static int
set_ld_preload(void)
{
	char *envval = getenv("LD_PRELOAD");
	if (!envval)
		return setenv("LD_PRELOAD", LIBFT, 0);

	int errsv = 0;

	int n = snprintf(NULL, 0, "%s:%s", LIBFT, envval);
	if (n < 0) {
		errsv = errno;
		goto error_snprintf;
	}

	char *buf = malloc(n + 1);
	if (!buf) {
		errsv = errno;
		goto error_malloc;
	}

	sprintf(buf, "%s:%s", LIBFT, envval);

	if (setenv("LD_PRELOAD", buf, 1) == -1) {
		errsv = errno;
		goto error_setenv;
	}

	free(buf);

	return 0;

error_setenv:
	free(buf);
error_malloc:
error_snprintf:
	errno = errsv;
	return -1;
}

static struct ft_shm *
ft_shm_create(const char *name)
{
	int errsv = 0;

	int fd = shm_open(name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		errsv = errno;
		goto error_shm_open;
	}

	if (ftruncate(fd, sizeof(struct ft_shm)) == -1) {
		errsv = errno;
		goto error_ftruncate;
	}

	void *addr = mmap(0, sizeof(struct ft_shm), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		errsv = errno;
		goto error_mmap;
	}
	struct ft_shm *shm = addr;

	pthread_mutexattr_t attr;
	if ((errsv = pthread_mutexattr_init(&attr)))
		goto error_mutexattr_init;
	if ((errsv = pthread_mutexattr_setpshared(
			     &attr, PTHREAD_PROCESS_SHARED)))
		goto error_mutexattr_setpshared;
	if ((errsv = pthread_mutex_init(&shm->mtx, &attr)))
		goto error_mutex_init;

	pthread_mutexattr_destroy(&attr);
	close(fd);

	return shm;

error_mutex_init:
error_mutexattr_setpshared:
	pthread_mutexattr_destroy(&attr);
error_mutexattr_init:
	munmap(addr, sizeof(struct ft_shm));
error_mmap:
error_ftruncate:
	close(fd);
error_shm_open:
	errno = errsv;
	return NULL;
}

static void
ft_shm_destroy(struct ft_shm *shm, const char *name)
{
	assert(shm);
	assert(name);

	shm_unlink(name);

	pthread_mutex_destroy(&shm->mtx);

	munmap(shm, sizeof(*shm));
}

// SPDX-License-Identifier: Apache-2.0
#ifndef FT_FT_H_
#define FT_FT_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#ifndef TIME_T_MAX
#define TIME_T_MAX \
	((time_t)((UINTMAX_C(1) << (sizeof(time_t) * CHAR_BIT - 1)) - 1))
#endif

#ifndef TIME_T_MIN
#if defined(__GNUC__) || defined(__clang__)
// GCC and LLVM support only two's complement signed integers.
#define TIME_T_MIN (-TIME_T_MAX - 1)
#else
#define TIME_T_MIN (-TIME_T_MAX)
#endif
#endif

#ifndef UTIME_T_MAX
#define UTIME_T_MAX (((uintmax_t)TIME_T_MAX << 1) + 1)
#endif

#ifndef TIMESPEC_MIN
#define TIMESPEC_MIN \
	(struct timespec) { TIME_T_MIN, 0 }
#endif

#ifndef TIMESPEC_MAX
#define TIMESPEC_MAX \
	(struct timespec) { TIME_T_MAX, 999999999l }
#endif

static struct timespec timespec_add(
		const struct timespec *tp, const struct timespec *inc);

static double strtorate(const char *nptr, char **endptr);
static struct timespec strtots(const char *nptr, char **endptr);

static int setenvrate(const char *envname, double value, int overwrite);
static inline int setenvts(
		const char *envname, const struct timespec *tp, int overwrite);

struct ft_clockattr {
	double rate;
	struct timespec begin;
	struct timespec offset;
};

#define FT_CLOCKATTR_INIT \
	{ \
		1., { 0, 0 }, { 0, 0 } \
	}

static void ft_clockattr_init(struct ft_clockattr *attr, clockid_t clockid,
		double rate, const struct timespec *offset);

struct ft_info {
	struct ft_clockattr realtime;
	struct ft_clockattr monotonic;
#ifdef CLOCK_MONOTONIC_RAW
	struct ft_clockattr monotonic_raw;
#endif
#ifdef CLOCK_REALTIME_COARSE
	struct ft_clockattr realtime_coarse;
#endif
#ifdef CLOCK_MONOTONIC_COARSE
	struct ft_clockattr monotonic_coarse;
#endif
#ifdef CLOCK_BOOTTIME
	struct ft_clockattr boottime;
#endif
#ifdef CLOCK_TAI
	struct ft_clockattr tai;
#endif
};

static void ft_info_init(struct ft_info *info);

struct ft_shm {
	pthread_mutex_t mtx;
	struct ft_info info;
};

static struct ft_shm *ft_shm_open(const char *name);
static void ft_shm_lock(struct ft_shm *shm);
static void ft_shm_unlock(struct ft_shm *shm);

static inline struct timespec
timespec_add(const struct timespec *tp, const struct timespec *inc)
{
	assert(tp);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);
	assert(inc);
	assert(inc->tv_nsec >= 0);
	assert(inc->tv_nsec < 1000000000l);

	uintmax_t tv_sec = (uintmax_t)tp->tv_sec + (uintmax_t)inc->tv_sec;
	long tv_nsec = tp->tv_nsec + inc->tv_nsec;
	if (tv_nsec >= 1000000000l) {
		tv_sec++;
		tv_nsec -= 1000000000l;
	}
	tv_sec &= UTIME_T_MAX;
	return (struct timespec){ tv_sec <= (uintmax_t)TIME_T_MAX
				? (time_t)tv_sec
				: -(time_t)((UTIME_T_MAX - tv_sec) + 1),
		tv_nsec };
}

static double
strtorate(const char *nptr, char **endptr)
{
	char *end = NULL;
	double val = strtod(nptr, &end);
	if (endptr)
		*endptr = end;
	if (end == nptr || !isfinite(val))
		return 1.;
	return val;
}

static struct timespec
strtots(const char *nptr, char **endptr)
{
	assert(nptr);

	const char *cp = nptr;

	while (isspace((unsigned char)*cp))
		cp++;

	int sign = 0;
	if (*cp == '+') {
		cp++;
	} else if (*cp == '-') {
		sign = 1;
		cp++;
	}

	if (!isdigit((unsigned char)*cp)) {
		if (endptr)
			*endptr = (char *)nptr;
		errno = EINVAL;
		return (struct timespec){ 0, 0 };
	}

	char *end = NULL;
	long long sec = strtoll(nptr, &end, 10);
	if (endptr)
		*endptr = end;
	assert(end > cp);
	cp = end;

	int overflow = 0;
	if ((sec == LLONG_MIN && errno == ERANGE)
			|| (sec == LLONG_MAX && errno == ERANGE)
			|| sec < TIME_T_MIN || sec > TIME_T_MAX)
		overflow = 1;

	long nsec = 0;
	if (*cp == '.' && isdigit((unsigned char)cp[1])) {
		cp++;
		nsec = strtol(cp, &end, 10);
		if (endptr)
			*endptr = end;
		assert(end > cp);

		assert(nsec >= 0);
		if (nsec > 999999999l) {
			errno = ERANGE;
			nsec = 999999999l;
		} else {
			while (end - cp < 9) {
				assert(nsec < 100000000l);
				nsec *= 10;
				end++;
			}
		}
	}
	assert(nsec < 1000000000l);

	if (!overflow && sign && nsec > 0) {
		if (sec > TIME_T_MIN) {
			sec--;
			nsec = 1000000000l - nsec;
		} else {
			overflow = 1;
		}
	}

	if (overflow) {
		errno = ERANGE;
		return sign ? TIMESPEC_MIN : TIMESPEC_MAX;
	}

	return (struct timespec){ sec, nsec };
}

static inline int
setenvrate(const char *envname, double value, int overwrite)
{
	char envval[32] = { 0 };
	if (snprintf(envval, sizeof(envval), "%.*g", DBL_DIG, value) < 0)
		return -1;
	return setenv(envname, envval, overwrite);
}

static inline int
setenvts(const char *envname, const struct timespec *tp, int overwrite)
{
	assert(tp);
	assert(tp->tv_nsec >= 0);
	assert(tp->tv_nsec < 1000000000l);

	long long sec = tp->tv_sec;
	long nsec = tp->tv_nsec;
	if (sec < 0 && nsec > 0) {
		sec++;
		nsec = 1000000000l - nsec;
	}

	char envval[32];
	if (nsec) {
		if (snprintf(envval, sizeof(envval), "%lld.%09ld", sec, nsec)
				< 0)
			return -1;
	} else {
		if (snprintf(envval, sizeof(envval), "%lld", sec) < 0)
			return -1;
	}
	return setenv(envname, envval, overwrite);
}

#ifndef FT_LIBFT_C_
static inline void
ft_clockattr_init(struct ft_clockattr *attr, clockid_t clockid, double rate,
		const struct timespec *offset)
{
	assert(attr);
	assert(offset);

	struct timespec now = { 0, 0 };
	clock_gettime(clockid, &now);

	*attr = (struct ft_clockattr){ rate, now, *offset };
}
#endif // !FT_LIBFT_C_

static inline void
ft_info_init(struct ft_info *info)
{
	assert(info);

	char *envval;
	double rate = 1.;
	if ((envval = getenv("FT_RATE")))
		rate = strtorate(envval, NULL);

	struct timespec offset = { 0, 0 };
	if ((envval = getenv("FT_OFFSET")))
		offset = strtots(envval, NULL);

	ft_clockattr_init(&info->realtime, CLOCK_REALTIME, rate, &offset);
	ft_clockattr_init(&info->monotonic, CLOCK_MONOTONIC, rate, &offset);
#ifdef CLOCK_MONOTONIC_RAW
	ft_clockattr_init(&info->monotonic_raw, CLOCK_MONOTONIC_RAW, rate,
			&offset);
#endif
#ifdef CLOCK_REALTIME_COARSE
	ft_clockattr_init(&info->realtime_coarse, CLOCK_REALTIME_COARSE, rate,
			&offset);
#endif
#ifdef CLOCK_MONOTONIC_COARSE
	ft_clockattr_init(&info->monotonic_coarse, CLOCK_MONOTONIC_COARSE, rate,
			&offset);
#endif
#ifdef CLOCK_BOOTTIME
	ft_clockattr_init(&info->boottime, CLOCK_BOOTTIME, rate, &offset);
#endif
#ifdef CLOCK_TAI
	ft_clockattr_init(&info->tai, CLOCK_TAI, rate, &offset);
#endif

	if ((envval = getenv("FT_REALTIME_RATE")))
		info->realtime.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_REALTIME_BEGIN")))
		info->realtime.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_REALTIME_OFFSET")))
		info->realtime.offset = strtots(envval, NULL);

	if ((envval = getenv("FT_MONOTONIC_RATE")))
		info->monotonic.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_BEGIN")))
		info->monotonic.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_OFFSET")))
		info->monotonic.offset = strtots(envval, NULL);

#ifdef CLOCK_MONOTONIC_RAW
	if ((envval = getenv("FT_MONOTONIC_RAW_RATE")))
		info->monotonic_raw.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_RAW_BEGIN")))
		info->monotonic_raw.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_RAW_OFFSET")))
		info->monotonic_raw.offset = strtots(envval, NULL);
#endif

#ifdef CLOCK_REALTIME_COARSE
	if ((envval = getenv("FT_REALTIME_COARSE_RATE")))
		info->realtime_coarse.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_REALTIME_COARSE_BEGIN")))
		info->realtime_coarse.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_REALTIME_COARSE_OFFSET")))
		info->realtime_coarse.offset = strtots(envval, NULL);
#endif

#ifdef CLOCK_MONOTONIC_COARSE
	if ((envval = getenv("FT_MONOTONIC_COARSE_RATE")))
		info->monotonic_coarse.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_COARSE_BEGIN")))
		info->monotonic_coarse.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_MONOTONIC_COARSE_OFFSET")))
		info->monotonic_coarse.offset = strtots(envval, NULL);
#endif

#ifdef CLOCK_BOOTTIME
	if ((envval = getenv("FT_BOOTTIME_RATE")))
		info->boottime.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_BOOTTIME_BEGIN")))
		info->boottime.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_BOOTTIME_OFFSET")))
		info->boottime.offset = strtots(envval, NULL);
#endif

#ifdef CLOCK_TAI
	if ((envval = getenv("FT_TAI_RATE")))
		info->tai.rate = strtorate(envval, NULL);
	if ((envval = getenv("FT_TAI_BEGIN")))
		info->tai.begin = strtots(envval, NULL);
	if ((envval = getenv("FT_TAI_OFFSET")))
		info->tai.offset = strtots(envval, NULL);
#endif
}

static inline struct ft_shm *
ft_shm_open(const char *name)
{
	int errsv = 0;

	int fd = shm_open(name, O_RDWR, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		errsv = errno;
		goto error_shm_open;
	}
	void *addr = mmap(0, sizeof(struct ft_shm), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		errsv = errno;
		goto error_mmap;
	}

	close(fd);

	return addr;

error_mmap:
	close(fd);
error_shm_open:
	errno = errsv;
	return NULL;
}

static inline void
ft_shm_lock(struct ft_shm *shm)
{
	if (shm)
		while (pthread_mutex_lock(&shm->mtx) == EINTR)
			;
}

static inline void
ft_shm_unlock(struct ft_shm *shm)
{
	if (shm)
		pthread_mutex_unlock(&shm->mtx);
}

#endif // FT_FT_H_
